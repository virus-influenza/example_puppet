define kidstoschool::service
(
	# service configuration
	# mservice unique descriptor name
	$mservice,
	# mservice version to be deployed
	$version      = "1.0",
	# functional description
	$description  = 'microservice',
	# user privileges
	$user         = "mservices",
	$group        = "mservices",

	# template configuration
	# [java,node]
	$service_type,
	# executable command line options
	$bin_opts  = "",
	# microservice command line options
	$service_opts = "",
	
	# systemctl service templates
	$service_template_file = "kidstoschool/systemctl_service.erb",
	$service_env_file      = "kidstoschool/systemctl_environment.erb",
	$service_base_path     = "/etc/systemd/system/",

){
	include kidstoschool::java
	include kidstoschool::nodejs

	# service template
	file { "assert systemctl descriptor for ${mservice} ${version} ${revision} present":
		path    => "${service_base_path}/${mservice}.service",
		ensure  => file,
		owner   => $user,
		group   => $group,
		mode    => 644,
		content => template("kidstoschool/systemctl_${service_type}_service.erb"),
	}
	
	# update systemctl daemon if service descriptor changed
        exec {"assert reload of systemctl daemon if template for ${mservice} changed":
                command => "systemctl --system daemon-reload",
                path    => ["/bin"],
                subscribe => File["assert systemctl descriptor for ${mservice} ${version} ${revision} present"],
        }

	# enable and start the service
        service { "assert ${mservice} ${version} ${revision} run":
	        name     => "${mservice}.service",
        	ensure   => running,
		enable   => true,
	        provider => systemd,
		subscribe  => File["assert systemctl descriptor for ${mservice} ${version} ${revision} present"],
        }
}

