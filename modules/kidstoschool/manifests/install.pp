define kidstoschool::install(

	$mservice,
	$revision    = 'unstable',
	$orig_repo   = "git@bitbucket.org:virus-influenza",
	$user        = 'mservices',
	$group       = 'mservices',
	$install     = '',
	$install_options = '',
){

	$path = "/home/${user}/repos/services/${mservice}"
        # assert path for microservice repository
        file { "assert /home/${user}/repos/services/${mservice}":
		path    => $path,
                ensure  => directory,
                owner   => $user,
                group   => $group,
                mode    => 644,
        } ->

	# requires the mservice repository updated
	vcsrepo { "assert ${mservice} ${version} ${revision} git repo latest":
		ensure   => latest,
		provider => git,
		source   => "${orig_repo}/${mservice}.git",
		revision => $revision,
		user     => $user,
		path     => $path,
	} ->

	exec { "install mservice ${mservice}":
		command => "${install} ${install_options}",
		cwd     => "/home/${user}/repos/services/${mservice}",
		path    => ["/bin","/usr/bin/"],
		user    => $user,
	} ->

	exec{ "flag ${mservice} to restart":
		command => "true",
		path    => ["/bin"],
	}

	service {"assert ${mservice} is running":
		name      => "${mservice}.service",
		ensure    => running,
	        provider  => systemd,
		subscribe => Exec["flag ${mservice} to restart"],
	} 

}
