class coderunner::service (
	$mservice,
){
	
	service { $mservice:
		name => "${mservice}.service",
		ensure => running,
		provider => systemd,
		require => Class['coderunner::build'],
	}
}
