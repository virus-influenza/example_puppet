class coderunner (
	$project = 'speiseplan-backend',
	$mservice = 'mfoodpoint',
	$user = 'mservices'
){

	$path  = "/home/${user}/repos/${project}/${mservice}"

	debug "mservice path for runner : ${path}"

	class { 'coderunner::build':
		path => $path,
	}

	class { 'coderunner::service':
		mservice => $mservice,
	}
}
