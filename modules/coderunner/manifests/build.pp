class coderunner::build (
	$path,
){
	debug "cwd in build : ${path}"

	exec{ 'build':
		command => "gradle build",
		cwd => "/home/mservices/repos/speiseplan-backend/mfoodpoint",
		path => "/root/.gvm/gradle/current/bin/:/usr/bin/:/usr/local/bin/:/bin/",
		notify => Class['coderunner::service'],
		user => 'root',
		require => Class['codeupdate::service'],
	}
}
