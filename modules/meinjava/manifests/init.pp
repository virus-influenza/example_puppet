class meinjava(
  #$distribution = 'jre',
  #$version = '7',
  $package = 'openjdk-7-jre-headless'
){
	# https://forge.puppetlabs.com/puppetlabs/java
	class { 'java':
  		#distribution => $distribution,
		  #version      => $version
      package => $package
	}
}
