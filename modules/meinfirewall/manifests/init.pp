class meinfirewall(

){

	# https://forge.puppetlabs.com/puppetlabs/firewall
	resources { 'firewall':
		purge => true
	}

#	resources { 'firewallchain':
# 		purge => true
#	}

#	Firewall {
#		before  => Class['meinfirewall::post'],
#		require => Class['meinfirewall::pre'],
#	}

#	class { ['meinfirewall::pre', 'meinfirewall::post']: }

	class { 'firewall': }
}
