class hosts::util::ipv6(

){

  # The following lines are desirable for IPv6 capable hosts
  # ::1     ip6-localhost ip6-loopback
  # fe00::0 ip6-localnet
  # ff00::0 ip6-mcastprefix
  # ff02::1 ip6-allnodes
  # ff02::2 ip6-allrouters

  host { 'ip6-localhost':
    ip           => '::1',
    host_aliases => ['ip6-loopback'],
  }

  host { 'ip6-localnet':
    ip           => 'fe00::0',
  }

  host { 'ip6-mcastprefix':
    ip           => 'ff00::0',
  }

  host { 'ip6-allnodes':
    ip           => 'ff02::1',
  }

  host { 'ip6-allrouters':
    ip           => 'ff02::2',
  }
}
