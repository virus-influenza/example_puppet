class hosts::speiseplan_b(
	
){
  require hosts::params::all

  class{'hosts::util::localhost':}->

  # self
  host { $hosts::params::speiseplan_b::hostname :
    ip           => $hosts::params::speiseplan_b::network_ip,
    host_aliases => $hosts::params::speiseplan_b::aliases,
  }->

  # network
  host { $hosts::params::speiseplan_a::hostname :
    ip           => $hosts::params::speiseplan_a::internal_ip,
    host_aliases => $hosts::params::speiseplan_a::aliases,
  }
}

