class hosts::params::speiseplan_b(
){
  # services
  $mysql    = 'mysql.speiseplan'
  $mongodb  = 'mongodb.speiseplan'
  $rabbitmq = 'rabbitmq.speiseplan'

  $hostname    = 'speiseplan-b'
  $internal_ip = '10.133.182.133'
  $network_ip  = '178.62.203.11'
  $aliases     = [
    $mysql,
    $mongodb,
    $rabbitmq
  ]
}