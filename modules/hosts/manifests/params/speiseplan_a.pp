class hosts::params::speiseplan_a(
){

  #microservices
  $mgimmy     = 'mgimmy.speiseplan'
  $microwave  = 'microwave.speiseplan'
  $api        = 'api.search.speiseplan'
  $mkitchenstore = 'mkitchenstore.speiseplan'
  $mconfig    = 'mconfig.speiseplan'
  $markimedes = 'markimedes.speiseplan'
  $mwolf      = 'mwolf.speiseplan'
  $wiremock   = 'wiremock.speiseplan'
  $kamaji     = 'kamaji.speiseplan'
  $kitchen_front = "kitchenfront.speiseplan"
  $portal_front  = "portalfront.speiseplan"
  $mauthentication  = "mauthentication.speiseplan"

  # hosts
  $hostname    = 'speiseplan-a'
  $internal_ip = '10.133.222.67'
  $network_ip  = '178.62.219.179'
  $aliases     = [
    $mgimmy,
    $api,
    $mkitchenstore,
    $mconfig,
    $markimedes,
    $mwolf
  ]
}