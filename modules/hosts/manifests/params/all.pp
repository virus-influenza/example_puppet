class hosts::params::all(
){
  ## convenience class to require every hosts parameters
  require hosts::params::speiseplan_a
  require hosts::params::speiseplan_b
}