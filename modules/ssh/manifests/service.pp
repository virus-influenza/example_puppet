class ssh::service(
){
	service { 'ssh.service':
		ensure => running,
		hasstatus => true,
		hasrestart => true,
		provider => 'systemd',
		require=> Class['ssh::install'],
	}
	
	# TODO: FIREWALL OPTIONS GO HERE
}
