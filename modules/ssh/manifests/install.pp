class ssh::install(
){
	package { 'ssh':
		ensure => present,
		name => 'openssh-server',
	}
}
