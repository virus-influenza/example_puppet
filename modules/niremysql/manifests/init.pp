class niremysql(

  $bind_ip = "127.0.0.1",
){


  # https://forge.puppetlabs.com/puppetlabs/mysql
  # [client]
  # port = 3306
  # socket = /var/run/mysqld/mysqld.sock
  # [mysqld]
  # bind-address = some.internal.ip.number
  class { '::mysql::server':
    root_password           => 'nak',
    remove_default_accounts => true,

    override_options        => {
      'client'  => {
        'port'  => '3306',
        'socket'=> '/var/run/mysqld/mysqld.sock',
      },
      'mysqld'  =>{
        'bind-address' => $bind_ip
      }
    }

  }

  mysql::db { 'micsa':
    user     => 'cobuys',
    password => 'cobuys',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'activity':
    user     => 'activity',
    password => 'cobuys',
    host     => '%',
    grant    => ['ALL'],
  }

}
