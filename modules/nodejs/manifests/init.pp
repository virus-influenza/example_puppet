class nodejs(
) {

  class { 'nodejs::install': }

  nodejs::package{ 'gulp':
    name    => 'gulp',
    options => '--global',
    require => Class['nodejs::install']
  }

}
