define nodejs::package(
  $name,
  $node_version,
  $options = "--global",
){

  # install global package
  exec{ "install global package ${name}":
    command => "/usr/bin/npm install ${options} ${name}",
    #path    => ["/usr/bin/"],
    user    => root,
    group   => root,
  }->

    # ln -ls /usr/bin/gulp /opt/binaries/nodejs/node-${version}-linux-x64/bin/gulp
  file { "/usr/bin/${name}":
    ensure => 'link',
    target => "/opt/binaries/node/node-${node_version}-linux-x64.tar.gz/node-${node_version}-linux-x64/bin/${name}",
  }
}
