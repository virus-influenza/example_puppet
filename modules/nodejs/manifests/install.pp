class nodejs::install(
  $version  = 'v0.12.2',
  $source   = 'http://nodejs.org/dist',
) {

  include archive::prerequisites

  # legacy
  # http://nodejs.org/dist/v0.12.2/node-v0.12.2-linux-x64.tar.gz
  # $download = "${source}/${version}/node-${version}-linux-x64.tar.gz"


  # http://nodejs.org/dist/latest-v5.x/node-v5.6.0-linux-x64.tar.gz
  $download = "${source}/node-${version}-linux-x64.tar.gz"

  notice(" MAY I HAVE YOUR ATENTION PLEASE ")
  notice($download)

  # test if there is an existing installation of the same gradle verion
  #exec { "test nodejs ${version} installed":
  #  command => '/bin/true',
  #  unless  => "/usr/bin/test -e /opt/binaries/node/node-${version}-linux-x64/",
  #}

  # if there is no installation, proceed to get the source
  archive { "node-${version}-linux-x64.tar.gz":
    ensure           => present,
    url              => $download,
    target           => '/opt/binaries/node/',
    checksum         => false,
    #require          => Exec["test nodejs ${version} installed"],
  } ->

  # link the source to the standard path /usr/bin/
    # DEPRECATED ln -ls /usr/bin/node /opt/binaries/nodejs/node-${version}-linux-x64/bin/node
    # DEPRECATED ln -ls /usr/bin/node /opt/binaries/node/node-${version}-linux-x64.tar.gz/node-v5.6.0-linux-x64
  file { '/usr/bin/node':
    ensure => 'link',
    target => "/opt/binaries/node/node-${version}-linux-x64.tar.gz/node-${version}-linux-x64/bin/node",
  } ->

    # ln -ls /usr/bin/npm /opt/binaries/node-${version}-linux-x64/bin/npm
  file { '/usr/bin/npm':
    ensure => 'link',
    target => "/opt/binaries/node/node-${version}-linux-x64.tar.gz/node-${version}-linux-x64/bin/npm",
  }

}
