class gradle::install(
	$version = '2.3',
	$source  = 'https://services.gradle.org/distributions', 

) {
  include archive::prerequisites

	# https://services.gradle.org/distributions/gradle-2.3-bin.zip
	# $download = 'https://services.gradle.org/distributions/gradle-2.3-bin.zip'
	$download = "${source}/gradle-${version}-bin.zip"

	# test if there is an existing installation of the same gradle verion
	exec {"test gradle ${version} installed":
		command => '/bin/true',
		unless  => "/usr/bin/test -e /opt/binaries/gradle/gradle-${version}/",
	}

	# if there is no installation, proceed to get the source
	archive { "gradle":
		ensure           => present,
		url              => $download,
		extension        => 'zip',
    target           => "/opt/binaries/",
		checksum         => false,
		require          => Exec["test gradle ${version} installed"],
	} ~>

	# link the source to the standard path /usr/bin/
	# ln -ls /usr/bin/gradle /opt/binaries/gradle-${version}/bin/gradle
	file { '/usr/bin/gradle':
		ensure => 'link',
		target => "/opt/binaries/gradle/gradle-${version}/bin/gradle",
	}
}
