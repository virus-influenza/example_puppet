class gradle(
  $version = 2.3
){

  require meinjava

  class{ 'gradle::install':
    version => $version,
    require => File["/opt/binaries"],
  }
}
