class microservices::java(
){
        kidstoschool::kid {
        'mconfig':
                mservice     => 'mconfig',
                description  => 'spring cloud config server',
                service_type => 'java',
                bin_opts     => '-jar -Xmx150m',
                service_opts => '--spring.profiles.active=prod';
        'mwolf':
                mservice     => 'mwolf',
                description  => 'amqp broker configuration microservice',
                service_type => 'java',
                bin_opts     => '-jar -Xmx250m',
                service_opts => '--spring.profiles.active=prod';
        'markimedes':
                mservice     => 'markimedes',
                description  => 'spring cloud netflix eureka service registry server',
                service_type => 'java',
                bin_opts     => '-jar -Xmx150m',
                service_opts => 'spring.profiles.active=prod';
        'mfoodpoint':
                mservice     => 'mfoodpoint',
                description  => 'rdbms food point microservice',
                service_type => 'java',
                bin_opts     => '-jar -Xmx250m',
                service_opts => '--spring.profiles.active=prod';
        'mgimmy':
                mservice     => 'mgimmy',
                description  => 'mangodb searchable food point microservice',
                service_type => 'java',
                bin_opts     => '-jar -Xmx250m',
                service_opts => '--spring.profiles.active=prod';
	}
}
