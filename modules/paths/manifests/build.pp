class paths::build(
  $base      = '/home/mservices/build',
  $java      = 'java',
  $node      = 'node',
){
# assert root path for repositories
  file { $base:
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
  } ->

  # assert path for java built services
  file { "${base}/${java}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
  }

# assert path for node built services
  file { "${base}/${node}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
  }
}

