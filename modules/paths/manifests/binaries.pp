class paths::binaries(
){
# assert root path for binaries
  file { "/opt/binaries":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }
}
