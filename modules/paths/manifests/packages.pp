class paths::packages(

){
# assert root path for binaries
  file { "/opt/packages":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }
}
