class paths::docker::registry(
  $base     = '/opt/docker',
  $registry = '/registry'
){

  # assert root path for docker registry data
  file { "${base}/${registry}":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }
}
