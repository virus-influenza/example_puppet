class paths::docker::base(
  $base      = '/opt/docker',
){
  # assert root path for docker
  file { $base:
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 0644,
  }
}
