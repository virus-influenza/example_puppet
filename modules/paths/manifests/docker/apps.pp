class paths::docker::apps(
  $base      = '/apps',
){

  # assert root path for apps
  file { $base:
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 0644,
  }
}
