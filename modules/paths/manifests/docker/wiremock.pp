class paths::docker::wiremock(
  $base      = '/',
  $wiremock  = 'wiremock',
  $files     = '__files',
  $mappings  = 'mappings'
){

# assert root path for wiremock
  file { "${base}/${wiremock}":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }

  # assert root path for wiremock files
  # they turn into a dahoood:house and dahood:link to get the json from repo
  file { "${base}/${wiremock}/${files}":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }

  # assert root path for wiremock mappings
  # they turn into a dahoood:house and dahood:link to get the json from repo
  file { "${base}/${wiremock}/${mappings}":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }

}

