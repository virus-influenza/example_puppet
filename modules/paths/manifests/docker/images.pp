class paths::docker::images(
  $base      = '/home/mservices/docker',
  $images    = 'images',
){
# assert root path for repositories
  file { $base:
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 0644,
  } ->

  # assert path for unit repositories
  file { "${base}/${images}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 0644,
  }
}
