class paths::wedoitbetter::bare_repositories(

){

  # assert root path for git
  file { "/home/git/bare":
    ensure  => directory,
    owner   => 'git',
    group   => 'git',
    mode    => 644,
  }

}
