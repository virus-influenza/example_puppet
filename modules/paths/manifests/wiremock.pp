class paths::wiremock(
){
# assert root path for binaries
  file { "/opt/wiremock":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }
}
