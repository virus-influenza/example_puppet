class paths::repositories(
  $base      = '/home/mservices/repos',
  $units     = 'units',
  $services  = 'services',
){
# assert root path for repositories
  file { $base:
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
    require => User["mservices"],
  }

# assert path for unit repositories
  file { "${base}/${units}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
    require => User["mservices"],
  }

# assert path for java microservices repositories
  file { "${base}/${services}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
    require => User["mservices"],
  }

}

