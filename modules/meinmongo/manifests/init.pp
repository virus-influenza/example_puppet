class meinmongo(

  $manage_package_repo = true,
  $port = 27017,
  $bind_ip,
){

  ## esto es muy cute pero no
  # notify { $::ipaddress_eth0: }
  # notify { $::ipaddress_eth1: }


  # mongodb https://forge.puppetlabs.com/puppetlabs/mongodb
  # install mongodb from 10Gen repo
  class { '::mongodb::globals':
    manage_package_repo => true,
  }->

  # configure
  class { '::mongodb::server':
    port => $port,
    bind_ip => [$bind_ip],

    # https://tickets.puppetlabs.com/browse/MODULES-534
    # auth => true,
  }->

  class { '::mongodb::client': }
}
