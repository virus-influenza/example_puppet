class meinwiremock::install(
  $version  = '1.57',
  $source   = 'http://repo1.maven.org/maven2/com/github/tomakehurst/wiremock/',
) {

# http://repo1.maven.org/maven2/com/github/tomakehurst/wiremock/1.57/wiremock-1.57-standalone.jar
  $download = "${source}/${version}/wiremock-${version}-standalone.jar"

  require paths::docker::wiremock


# get the fucking file
  include wget
  wget::fetch { "asssert download ${download}":
    source      => $download,
    destination => "/wiremock/wiremock-stable.jar",
    timeout     => 0,
    verbose     => false,
  }

}
