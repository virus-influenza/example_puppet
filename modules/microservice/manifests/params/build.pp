class microservice::params::build(
){
  $origin_repository = "git@bitbucket.org:virus-influenza"
  $user              = 'mservices'
  $group             = 'mservices'
  $source            = "/home/${user}/repos/services"
  $build_folder      = "/build/libs"
  $node_build_folder = "/dist/"
  $microwave_build_folder = "./"
}
