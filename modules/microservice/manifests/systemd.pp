define microservice::systemd(

        # service configuration
	#
        # mservice unique descriptor name
        $mservice,
	$revision     = 'unstable',

        # functional description
        $description  = 'microservice',

        # user privileges
        $user         = microservice::params::build::user,
        $group        = microservice::params::build::group,

        # template configuration
	#
        # [java,node]
        $service_type,
        $build,
        $build_options,

        # executable command line options
        $bin_opts  = "",
        # microservice command line options
        $service_opts = "",

        # systemctl service templates
        $service_base_path     = "/etc/systemd/system/",
){
 
        include microservice::params::java
        include microservice::params::node

	$path = "/home/mservices/build/${service_type}/${mservice}/${revision}"

	file { ["/home/mservices/build/", 
		"/home/mservices/build/${service_type}", 
		"/home/mservices/build/${service_type}/${mservice}",
		"${path}"]:
		
		ensure => directory,
		user   => $user,
		group  => $group,
		mode   => 0644,
	} ->

        microservice::util::build { "${mservice}":
                mservice      => $mservice,
                revision      => $revision,
                build         => $build,
                build_options => $build_options,
                deliver_to    => "${path}"
        } ->

        # service template
        file { "assert systemctl descriptor for ${mservice} ${revision} present":
                path    => "${service_base_path}/${mservice}.service",
                ensure  => file,
                owner   => $user,
                group   => $group,
                mode    => 644,
                content => template("microservice/systemctl_${service_type}_service.erb"),
        }

        # update systemctl daemon if service descriptor changed
        exec {"assert reload of systemctl daemon if template for ${mservice} changed":
                command => "systemctl --system daemon-reload",
                path    => ["/bin"],
                subscribe => File["assert systemctl descriptor for ${mservice} ${revision} present"],
        }

        # enable and start the service
        service { "assert ${mservice} ${version} ${revision} run":
                name     => "${mservice}.service",
                ensure   => running,
                enable   => true,
                provider => systemd,
                subscribe  => File["assert systemctl descriptor for ${mservice} ${revision} present"],
        }
}
