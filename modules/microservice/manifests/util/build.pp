# asserts repository path
#	asserts repository revision up to date
#	executes build command in repository
#
define microservice::util::build(

  $mservice,
  $service_type  = '',
  $revision      = 'unstable',
  $build         = '',
  $build_opts    = '',
  $deliver_to    = '/tmp',

  $origin        = ''
){

  include microservice::params::build

  # pre refactor i dont want to break virus-influenza builds
  if( $origin == ''){
    $orig_repo     = "${microservice::params::build::origin_repository}/${mservice}.git"
  } else{
    $orig_repo     = $origin
  }

  $user          = $microservice::params::build::user
  $group         = $microservice::params::build::group
  $source        = $microservice::params::build::source

  # until refactor is finished i dont want to break every java build :)
  if ($service_type == ''){
    $build_folder  = $microservice::params::build::build_folder
  }

  if($service_type == "node"){
    $build_folder  = $microservice::params::build::node_build_folder
  }

  if($service_type == "microwave"){
    $build_folder  = $microservice::params::build::microwave_build_folder
  }

  # assert path for microservice repository
  file { "assert ${source}/${mservice}":
    path    => "${source}/${mservice}",
    ensure  => directory,
    owner   => $user,
    group   => $group,
    mode    => 644,
  } ->

  # requires the mservice repository updated
  vcsrepo { "assert ${mservice} ${revision} git repo latest":
    ensure   => latest,
    provider => git,
    source   => $orig_repo,
    revision => $revision,
    user     => $user,
    path     => "${source}/${mservice}",
  } ->

  # executes build command in service repository path
  exec { "assert built mservice ${mservice} ${revision}":
    command => "${build} ${build_opts}",
    cwd     => "/home/${user}/repos/services/${mservice}",
    path    => ["/bin","/usr/bin/"],
    timeout => 0,
    user    => $user,
  } ->

    # delivers the package to specified path
  file { "assert build mservice ${mservice} ${revision} delivered to ${deliver_to}":
    path    => "${deliver_to}",
    source  => "${source}/${mservice}/${build_folder}",
    ensure  => directory,
    recurse => true,
    purge   => true,
    force   => true,
    owner   => $user,
    group   => $group,
  }
}

