class users::root(
){
# user root
  user{ 'root':
    ensure => present,
  }
  group { 'root':
    ensure => present,
  }

  # root accepted keys
  # cell-one laptop
  ssh_authorized_key { 'virus@cell-one':
    user => 'root',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC+Wg6Q/OmhqY8BlFfV9FO+9D2pIV6LJkgOLiAF0rZzjcNurO8hUc/KWWoQG+i/mE+ma1CriIOiFq0LJ5oNgWAda6EBVuoaQjBkNuRQOjikzDnMR07tq8oJsnfguepDao408F7x9I6KyZIeo/uD1JYqh+WL1PzigbUWqqUI7crJQ8As8xzvoaZIbEScjVO85csaifubQbWZXRuQWBSSWCAlGTOpeQbRdXGu1FGqfvJkV8I58Bnd2wE9Z6UMUSppK+mKfmdKE8E1SXP/MsVbxYxK5LIhrDaXwuELIRBhHLAHee9F0qn8YlRhuXYypXgVevskUXLHbo2QbaZeC/1e2uDX',
  }

  # nak laptop
  ssh_authorized_key { 'nak@msi-cx70':
    user => 'root',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCxQWr6TS/lqSYSBZFzM+X7ZWSTWigbexBKMtJ3GWzzPn3DuQUk5FK1Tw1DWxA9J2hob6H97bKB9FBjw2xJYaD9IC+ZrD4ExXL92JJin7ApGVsNYzNtmH1vjXmRp70iePUGh3G3xtVcFHO/qSmudkRj2nKaKr5pCsHJfDNIMsKFfZd60V5ODx+u3OWzGhWeerOwBxRvHO7Y28d6DKXqIWXDDoCAKqfPcIgfxDHFoOEYidHuqyqX8UvPvVwCse6BzmB2r6YeB5fIU8I6udx707Lb71Yh23ramQarYKb26ceWHkOruVnI8PAP/jeudhy8CJmTJO56BGqcxYcBU73Ddfvl',
  }

  # envy
  ssh_authorized_key { 'pakito@envy':
    user => 'root',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC50AYpPbpLEVQojWJQesxvSUbjmVehzgJJaSxngvP9cSTv/tOFr033RjK2E5Tf3TUIjpETRFShHRnFbXOM9LtCErezNKo9vJGoBC8J78ouYO+qNg3+y07oZcyXI2kC0dMt5TA6DRSmbKknZOsQe9dJjw9ZolKmJxXM1c5jNjoRKxT17plkU4MNv0wdvK4ylxCjcRyBRzd0r3rPHKuwN6hPignrVHcO55GoLv16i6OQe5BlI0gyqnp+h4X5wrEr6mbSzsjIkjq9O0OSVldTUkJulfeLYITjW/HYSVdndq4lNSnyZIhTkASRyVTs5CelZ+tDXbDFAra9EkIzAu+wyOyz',
  }

  # motoko
  ssh_authorized_key { 'pakito@motoko':
    user => 'root',
    type => 'ssh-rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC/U2RSdvjr8LqC7hy0+azsSmORt2iB/Vl6PMFtPlYTc+5MB0QowVA6J2ze3WLlx6SrRWFncL+pSscIx68oMZevXv0Pe2W+qdeFqIaqXTP7oDITPEqpXqo2neyEwzxcfX8NTrjQi+FCU4z6EEew14FhnNLwMrs4a9aJmDI5XwZWnXhctuzIYYTNrMw7zrbITkAxuOd2OBYXc7sBiNp6z4+2PSIG6jf9VgLAVS+fIUsrxtvLTyJkcwuYlH3jIzOpr42wm69uIfGPx/pLoYxpaCZJGu6np9rYzYBb2rBb34rLnQ72CyS+SKZ6DikSxxOuAkthffBJLnhYaD0pfU/eTKtv'
  }

  # kamaji unit
  ssh_authorized_key { 'fabric@kamaji.speiseplan':
    user => 'root',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDUZc49Pyz/opTJ01xVG2t47uLW3HsYra19uPa4U4h3jlOeJvyepDgo02gKsVUV8CiBB0MfcmJx54REq6/BvR1NiKOXW2ZJRtMfxYsIbErYa1tamwUAomkdaS1U+m2JTBbdxUNfMTi15CQrcEqtllhL5pWuV36zyt7ZFSnqx80cOlGpA31MdVM+9sD2+sV7ENKXX/lUfnTZXqd8BCNQb8sAyIeIrgDMNy+IW1dQZNiA4b2NvkzGNyceaJDHQrAU6p8+Fhm8cRf9AeLl5o0OqZD6QTrCV0KB14xDVdYuj3m+hHcZ9NbRRIz1mAixWrkjlgTTuEE5IKsO8lgTNtC+FRq/',
  }

}
