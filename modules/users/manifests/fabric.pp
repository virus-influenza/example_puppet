class users::fabric(
){
# user fabric
  user{ 'fabric':
    ensure => present,
  } ~>

  group { 'fabric':
    ensure => present,
  } ~>

  file{ '/home/fabric':
    ensure => directory,
    owner  => 'fabric',
    mode   => 0644,
  } ~>

  file{ '/home/fabric/.ssh':
    ensure => directory,
    owner  => 'fabric',
    mode   => 0644,
  } ~>

  # id rsa
  file { "fabric private key":
    ensure  => 'file',
    path    => "/home/fabric/.ssh/id_rsa.fabric",
    content => file('users/fabric/id_rsa.fabric'),
    owner   => 'fabric',
    group   => 'fabric',
    mode    => 0600,
  } ~>

  # id rsa.pub
  file { "fabric public key":
    ensure => 'file',
    path    => "/home/fabric/.ssh/id_rsa.fabric.pub",
    content => file('users/fabric/id_rsa.fabric.pub'),
    owner   => 'fabric',
    group   => 'fabric',
    mode    => 0633,

  } ~>

  # .ssh/config use deployment key-> bitbucket host
  file { "fabric key configuration":
    ensure  => 'file',
    path    => "/home/fabric/.ssh/config",
    content => file('users/fabric/config'),
    owner   => 'fabric',
    group   => 'fabric',
    mode    => 0600,
  } ~>

  # include known host for fabric
  file { "fabric known hosts configuration":
    ensure  => 'file',
    path    => "/home/fabric/.ssh/known_hosts",
    content => file('users/fabric/known_hosts'),
    owner   => 'fabric',
    group   => 'fabric',
    mode    => 0600,
  }

}
