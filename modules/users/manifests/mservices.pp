class users::mservices(
){
# user mservices
  user{ 'mservices':
    ensure => present,
  } ~>

  group { 'mservices':
    ensure => present,
  } ~>

  file{ '/home/mservices':
    ensure => directory,
    owner  => 'mservices',
    mode   => 0644,
  } ~>

  file{ '/home/mservices/.ssh':
    ensure => directory,
    owner  => 'mservices',
    mode   => 0644,
  } ~>

  # id rsa
  file { "deployment private key":
    ensure  => 'file',
    path    => "/home/mservices/.ssh/id_rsa.deployment",
    content => file('users/mservices/id_rsa.deployment'),
    owner   => 'mservices',
    group   => 'mservices',
    mode    => 0600,
  } ~>

  # id rsa.pub
  file { "deployment public key":
    ensure => 'file',
    path    => "/home/mservices/.ssh/id_rsa.deployment.pub",
    content => file('users/mservices/id_rsa.deployment.pub'),
    owner   => 'mservices',
    group   => 'mservices',
    mode    => 0633,

  } ~>

  # .ssh/config use deployment key-> bitbucket host
  file { "deployment key configuration":
    ensure  => 'file',
    path    => "/home/mservices/.ssh/config",
    content => file('users/mservices/config'),
    owner   => 'mservices',
    group   => 'mservices',
    mode    => 0600,
  } ~>

  file { "bitbucket known hosts configuration":
    ensure  => 'file',
    path    => "/home/mservices/.ssh/known_hosts",
    content => file('users/mservices/known_hosts'),
    owner   => 'mservices',
    group   => 'mservices',
    mode    => 0600,
  }

# mservices accepted keys
# ssh_authorized_key {}
}
