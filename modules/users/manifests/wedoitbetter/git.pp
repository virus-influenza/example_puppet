class users::wedoitbetter::git(
){
  # user git
  user{ 'git':
    ensure => present,
  } ->

  group { 'git':
    ensure => present,
  } ->

  file{ '/home/git':
    ensure => directory,
    owner  => 'git',
    mode   => 0644,
  } ->

  file{ '/home/git/.ssh':
    ensure => directory,
    owner  => 'git',
    mode   => 0644,
  } ->

  # git accepted keys
  # pakito cell-one laptop
  ssh_authorized_key { 'virus':
    user => 'git',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC+Wg6Q/OmhqY8BlFfV9FO+9D2pIV6LJkgOLiAF0rZzjcNurO8hUc/KWWoQG+i/mE+ma1CriIOiFq0LJ5oNgWAda6EBVuoaQjBkNuRQOjikzDnMR07tq8oJsnfguepDao408F7x9I6KyZIeo/uD1JYqh+WL1PzigbUWqqUI7crJQ8As8xzvoaZIbEScjVO85csaifubQbWZXRuQWBSSWCAlGTOpeQbRdXGu1FGqfvJkV8I58Bnd2wE9Z6UMUSppK+mKfmdKE8E1SXP/MsVbxYxK5LIhrDaXwuELIRBhHLAHee9F0qn8YlRhuXYypXgVevskUXLHbo2QbaZeC/1e2uDX',
  }

  # pakito envy 8cores
  ssh_authorized_key { 'pakito':
    user => 'git',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC50AYpPbpLEVQojWJQesxvSUbjmVehzgJJaSxngvP9cSTv/tOFr033RjK2E5Tf3TUIjpETRFShHRnFbXOM9LtCErezNKo9vJGoBC8J78ouYO+qNg3+y07oZcyXI2kC0dMt5TA6DRSmbKknZOsQe9dJjw9ZolKmJxXM1c5jNjoRKxT17plkU4MNv0wdvK4ylxCjcRyBRzd0r3rPHKuwN6hPignrVHcO55GoLv16i6OQe5BlI0gyqnp+h4X5wrEr6mbSzsjIkjq9O0OSVldTUkJulfeLYITjW/HYSVdndq4lNSnyZIhTkASRyVTs5CelZ+tDXbDFAra9EkIzAu+wyOyz',
  }

  # nak laptop
  ssh_authorized_key { 'nak':
    user => 'git',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCxQWr6TS/lqSYSBZFzM+X7ZWSTWigbexBKMtJ3GWzzPn3DuQUk5FK1Tw1DWxA9J2hob6H97bKB9FBjw2xJYaD9IC+ZrD4ExXL92JJin7ApGVsNYzNtmH1vjXmRp70iePUGh3G3xtVcFHO/qSmudkRj2nKaKr5pCsHJfDNIMsKFfZd60V5ODx+u3OWzGhWeerOwBxRvHO7Y28d6DKXqIWXDDoCAKqfPcIgfxDHFoOEYidHuqyqX8UvPvVwCse6BzmB2r6YeB5fIU8I6udx707Lb71Yh23ramQarYKb26ceWHkOruVnI8PAP/jeudhy8CJmTJO56BGqcxYcBU73Ddfvl',
  }

}
