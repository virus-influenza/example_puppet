class codeupdate{ 
	define mservice(
		$project = 'speiseplan-backend',

		$user  = 'mservices',
		$group = 'mservices',

		$repo = 'git@bitbucket.org:virus-influenza/speiseplan-backend.git',
		$tag_name = undef,
		$branch_name = 'unstable',

		$mservice = 'mfoodpoint',
		){
			$path  = "/home/${user}/repos/${project}"

			codeupdate::install::install { "ensure user, group and git" :
				user => $user,
				group => $group,
			}

			codeupdate::config::config { "ensure path owned by user" :
				path => $path,
				user => $user,
				group => $group
			}

			codeupdate::repo::repo { "ensure git repo in path":
				path => $path,
				user => $user,
				repo => $repo,
				tag_name => $tag_name,
				branch_name => $branch_name,
			}

			codeupdate::build::build { "ensure service is built":
				path => "${path}/${mservice}",
			}

			codeupdate::runner::runner { "ensure service is running":
				mservice => $mservice,
			}

		}
}
