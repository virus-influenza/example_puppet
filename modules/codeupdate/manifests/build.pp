class codeupdate::build {
	define build(
		$path,
	){
		exec{'update repo':
			command => "git pull",
			cwd => $path,
			path => "/usr/bin/:/usr/local/bin/:/bin/",
			user => "mservices",

		}

		exec{ 'build':
			command => "gradle install",
			cwd => $path,
			path => "/root/.gvm/gradle/current/bin/:/usr/bin/:/usr/local/bin/:/bin/",
			notify => Class['codeupdate::runner'],
			user => 'root',
			require => Class['codeupdate::repo'],
			unless => "git diff --quiet ORIG_HEAD.." # bibidi babidi bu :)
		}
	}
}
