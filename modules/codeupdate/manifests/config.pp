class codeupdate::config{ 
	define config (
		$path,
		$user,
		$group
	) {
		file { 'mservice repo' :
			path    => $path,
			ensure  => "directory",
			owner   => $user,
			group   => $group,
			mode    => 750,
			require => Class['codeupdate::install'],
			notify  => Class['codeupdate::repo'],
		}
	}
}
