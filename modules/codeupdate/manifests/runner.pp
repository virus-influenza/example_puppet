class codeupdate::runner{ 
	define runner(
		$mservice,
	){
		
		service { $mservice:
			name => "${mservice}.service",
			ensure => running,
			provider => systemd,
			status => "systemctl status ${mservice}.service",
			start => "systemctl start ${mservice}.service",
			stop => "systemctl stop ${mservice}.service",
			require => Class['codeupdate::build'],
		}
	}
}
