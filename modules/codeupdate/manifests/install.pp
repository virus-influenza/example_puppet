class codeupdate::install{
	(
		$user,
		$group
	){
		#ensure git is installed
		include git	
		
		# require user and group to be present
		user { $user:
			ensure => present,	
		}

		group { $group:
			ensure => present,
		}
	}
}
