class codeupdate::repo{
	define:repo(
		$path,
		$user,
		$repo,
		$tag_name,
		$branch_name,
	){
		if ( $tag_name ) {
			$target = $tag_name
		}else{
			$target = $branch_name
		}

		vcsrepo { $path:
			ensure   => present,
			provider => git,
			source   => $repo,
			revision => $target,
			user     => $user,
			require => Class['codeupdate::config'],
			notify => Class['codeupdate::build'],
		}
	}
}
