define dahood::exec
(
  $unit,
  $exec_command,
  $exec_opts,
  $pwd,
  $path = ["/bin","/usr/bin"]
){
  exec { "execute command: ${exec_command} with opts: ${exec_opts} in pwd: ${pwd} for unit: ${unit}":
    command => "$exec_command $exec_opts",
    cwd     => $pwd,
    path    => $path
  }
}
