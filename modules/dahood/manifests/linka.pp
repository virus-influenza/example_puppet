define dahood::linka
(
	$link,
	$target
){
	file { $link:
	   ensure => 'link',
	   target => $target,
	   force  => yes,
	}
}
