define dahood::copy
(
  $destination,
  $source,

  $user = 'mservices',
  $group = 'mservices'
){

  file { "assert ${source} is recursively copied into ${destination}":
    path    => $destination,
    ensure  => directory,
    recurse => true,
    purge   => true,
    force   => true,
    source  => $source,
    owner   => mservices,
    group   => mservices,
    links   => follow,
  }
}

