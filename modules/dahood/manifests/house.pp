define dahood::house
(
# house configuration
# unit unique descriptor name
  $unit,
# functional description
  $description = 'micro unit',
# origin repository
  $orig_repo   = "git@bitbucket.org:virus-influenza/%UNIT%.git",
# unit revision to be deployed
  $revision    = 'unstable',
# user and group for the units family
  $user        = "mservices",
  $group       = "mservices",
  $link_to     = "",
){
# house path
  $repo_path   = "/home/${user}/repos/units/${unit}"

# expand string patterns
  $orig_repo_expanded = regsubst($orig_repo,'%UNIT%', $unit)

  notice('CREATING REPO AND CLONING ')
  notice($orig_repo_expanded)
  notice($repo_path)


# require the mservices repository folder present
  file { "assert ${unit} ${revision} repository path present":
    path     => "${repo_path}",
    ensure   => "directory",
    owner    => $user,
    group    => $group,
    mode     => 644,
    require  => File["/home/${user}/repos"]
  }->

# requires the unit repository present in the latest commit for specified revision
  vcsrepo { "assert ${unit} ${revision} repository present":
    ensure   => latest,
    provider => git,
    source   => $orig_repo_expanded,
    revision => $revision,
    user     => $user,
    path     => "${repo_path}",
    require  => File["assert ${unit} ${revision} repository path present"]
  }
}
