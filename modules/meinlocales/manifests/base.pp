class meinlocales::base(
){

  # https://forge.puppetlabs.com/saz/locales
  # By default, en and de locales will be generated.
  class { 'locales':
    locales         => ['en_US.UTF-8 UTF-8', 'de_DE.UTF-8 UTF-8'],
    default_locale  => 'en_US.UTF-8',
    lc_all          => 'en_US.UTF-8',
  }

}