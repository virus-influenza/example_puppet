class meinmysql(

  $bind_ip = "0.0.0.0",
){


  # https://forge.puppetlabs.com/puppetlabs/mysql
  # [client]
  # port = 3306
  # socket = /var/run/mysqld/mysqld.sock
  # [mysqld]
  # bind-address = some.internal.ip.number
  class { '::mysql::server':
    root_password           => '_levit4!!!',
    remove_default_accounts => true,

    override_options        => {
      'client'  => {
        'port'  => '3306',
        'socket'=> '/var/run/mysqld/mysqld.sock',
      },
      'mysqld'  =>{
        'bind-address' => $bind_ip
      }
    }

  }

  mysql::db { 'speiseplan_mkitchenstore':
    user     => 'daneel',
    password => '_levit4!!!',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'speiseplan_mauthentication':
    user     => 'daneel',
    password => '_levit4!!!',
    host     => '%',
    grant    => ['ALL'],
  }

}
