class roles::ignasi_box(
){

    # default paths, /opt/packages and /opt/binaries
  class{ 'paths': } ->

    # chromium
  # class { 'nirechromium': }

    # code tools
    # java
  class{ 'meinjava':
    package => 'openjdk-7-jdk'
  } ->

  class{ 'gradle': } ->

    # mangodb
  class { 'meinmongo':
    bind_ip => ['127.0.0.1'],
    port    => 27017,
  } ->

    # mysql
  class { 'niremysql':
    bind_ip => '127.0.0.1'
  } ->

    # rabbitmq
  class{ 'meinrabbit':
    bind_ip           => '127.0.0.1',
    delete_guest_user => false
  }
}
