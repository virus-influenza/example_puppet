class roles::docker_microservices_box(
){

  # host file for machine speiseplan_a
  class {'hosts::speiseplan_a':} ->

  # host state
  class {'profiles::docker::base::host':} ->

  # docker services running in host
  # docker repository registry
  class {'profiles::docker::images::registry::service':} ->
  # wiremock
  class {'profiles::docker::images::wiremock::service':} ->

  # spring cloud mservices
  # distributed configuration
  class {'profiles::docker::images::mconfig::service':} ->
  # eureka service registry
  class { 'profiles::docker::images::markimedes::service': }
}