# build_box must be a super machine dedicated to just build fucking spring boot jars and their docker containers
# can not see the purpose of this rol :\
class roles::build_box(
){
  # mongodb
  # installs microservice in maven artifact repository
  class { 'profiles::docker::images::mgimmy::package': } ->
    # builds docker image for microservice:
    # 1) create build folder with puppet modules and Dockerfile
    # 2) executes docker build and docker push, creating the docker image and pushing it to the registry
  class { 'profiles::docker::images::mgimmy::build': } ->

    # rdbms
  class { 'profiles::docker::images::mkitchenstore::package': } ->
  class { 'profiles::docker::images::mkitchenstore::build': } ->

    # spring cloud microservices
    # eureka service registry
  class { 'profiles::docker::images::markimedes::service': } ->
    # distributed config server
  class { 'profiles::docker::images::mconfig::service': } ->

    # multi purposed http server ?
  class { 'profiles::docker::images::kamaji_unit::service': }

}