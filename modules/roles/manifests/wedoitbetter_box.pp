class roles::wedoitbetter_box(
){


  # root keys, git, java, puppet base code
  class{'profiles::wedoitbetter::base': }->

  # create repositories
  class{'profiles::wedoitbetter::repositories': }

}