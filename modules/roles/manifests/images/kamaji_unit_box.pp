class roles::images::kamaji_unit_box(

){

  # base fabric instalation with fabric repository available in /fabric
  class {"profiles::fabric::base": }->

  # packages and distributes the microservice
  class {"profiles::docker::images::kamaji_unit::package": }
}