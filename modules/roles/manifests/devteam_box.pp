class roles::devteam_box(
){

  # base fabric instalation with fabric repository available in /fabric
  class { "profiles::fabric::base": }->

    # base profile for develeopment
    # includes git, java, gradle, node, fabric
    # includes mservices user
  class { 'profiles::devteam::base': }
}
