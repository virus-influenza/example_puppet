class roles::docker_microservices_box(
){

  # host file for machine speiseplan_a
  class {'hosts::speiseplan_a':} ->

  # host state
  class { 'profiles::docker::base::host': } #->

  # mservices
  # ddbms
  #class {'profiles::docker::images::mgimmy::service':} ->
  # rdbms
  #class {'profiles::docker::images::mkitchenstore::service':} ->
  # eureka service registry
  #class {'profiles::docker::images::markimedes::service':} ->
  # multi purposed http server
  #class {'profiles::docker::images::kamaji_unit::service':}
}