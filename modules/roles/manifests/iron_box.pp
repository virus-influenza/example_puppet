class roles::iron_box(

){

  # users, puppet and firewall
  class {'profiles::iron::base': } ->

  # mongodb server, client; db and user access
  class {'profiles::iron::ddbms': } ->

  # rabbitmq server; user access
  class {'profiles::iron::amqp': } ->

  # mysql sever; db and user access
  class {'profiles::iron::rdbms': }

}
