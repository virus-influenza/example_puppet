define meindocker::image::build(
  $image,
  $user,
  $flavour ,
  $dockerfile_dir,
  $registry,
  $no_cache = '--no-cache'
){

  # https://blog.codecentric.de/en/2014/02/docker-registry-run-private-docker-image-repository/
  include docker::params

  # just to avoid problem with local mint installation
  if $::os {
    $docker_command = $docker::params::docker_command
  }else{
    $docker_command = "docker"
  }

  $image_build_command = "${docker_command} build  ${no_cache} -t $user/${image}${flavour} ${dockerfile_dir}"
  $image_tag_command   = "${docker_command} tag -f $user/${image}${flavour} ${registry}/$user/${image}${flavour}"
  $image_push_command  = "${docker_command} push ${registry}/$user/${image}${flavour}"

  exec { $image_build_command:
    path    => ['/bin', '/usr/bin'],
    timeout => 0,
  } ->

  exec { $image_tag_command:
    path    => ['/bin', '/usr/bin'],
    timeout => 0,
  } ->

  exec { $image_push_command:
    path    => ['/bin', '/usr/bin'],
    timeout => 0,
  }

}