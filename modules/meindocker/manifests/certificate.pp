class meindocker::certificate(
  $host,
  $port,
){

  include paths::docker::base # dunno if needed but it is a dependency

  # https://github.com/docker/distribution/blob/master/docs/deploying.md

  # 1) https://github.com/docker/distribution/blob/master/docs/deploying.md#3-use-a-self-signed-certificate-and-configure-docker-to-trust-it
  # configure docker service to trust the self signed certificated
  # /etc/docker/certs.d/myregistrydomain.com:5000/ca.crt
  $host_port = "${host}:${port}"

  file { ['/etc/docker/certs.d',
    "/etc/docker/certs.d/${host_port}"] :
    ensure => directory,
    owner  => root,
    mode   => 0644,
  } ->

  file { "server self signed certificate for docker registy access":
    path    => "/etc/docker/certs.d/${host_port}/ca.crt",
    content => file('meindocker/domain.crt'),
    owner   => root,
    group   => root,
    mode    => 0644,
  }

  # https://github.com/docker/distribution/blob/master/docs/deploying.md#1-buy-a-ssl-certificate-for-your-domain
  # create a certificate repository to use it in the docker registry container
  file { 'certificate repository' :
    path   => '/opt/docker/certs',
    ensure => directory,
    owner  => root,
    mode   => 0644,
  } ->

  file { 'certificate crt' :
    path    => "/opt/docker/certs/domain.crt",
    content => file('meindocker/domain.crt'),
    owner   => root,
    group   => root,
    mode    => 0644,
  } ->

  file { 'certificate key' :
    path    => "/opt/docker/certs/domain.key",
    content => file('meindocker/domain.key'),
    owner   => root,
    group   => root,
    mode    => 0644,
  }

}
