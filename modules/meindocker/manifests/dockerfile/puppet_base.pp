define meindocker::dockerfile::puppet_base(
  $image,
  $version,
  $base    = "/home/mservices/docker/images/", # param
){

  # assert path for dockerfile
  file { "${base}/${image}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
  }->

  file { "create dockerfile descriptor ${image}":
    path    => "${base}/${image}/Dockerfile",
    ensure  => file,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
    content => template("meindocker/dockerfile_puppet_base.erb"),
  }
}
