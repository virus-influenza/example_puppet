# service means a external packaged service, node, java, wiremock...
define meindocker::dockerfile::service_base(
  $image,
  $flavour,
  $base        = "/home/mservices/docker/images/", # param

  $puppet_code,
  $modules     = "/modules",
  $from        = "docker_base_box:unstable",
  $template    = "dockerfile_service_base.erb",

  $port,
){

  # assert path for dockerfile
  file { "${base}/${image}":
    ensure  => directory,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
  }->

  file { "assert docker image ${image} build path contains puppet /modules":
    path    => "${base}/${image}/modules",
    ensure  => directory,
    recurse => true,
    purge   => true,
    force   => true,
    source  => "${modules}",
    owner   => mservices,
    group   => mservices,
    links   => follow,
  }->

  file { "create dockerfile descriptor ${image}":
    path    => "${base}/${image}/Dockerfile",
    ensure  => file,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
    content => template("meindocker/${$template}"),
  }
}
