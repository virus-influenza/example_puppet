define meindocker::dockerfile::mservice(
  $name,
  $flavour,
  $service_type,
  $puppet_code,
  $port,
  $bin_opts,
  $service_opts,
  $base    = "/home/mservices/docker/images/", # param
  $modules = "/modules",
){

  require microservice::params::java
  require microservice::params::node

  # package must assert this folder, sorry :)
  # assert path for dockerfile
  #file { "${base}/${name}":
  #  ensure  => directory,
  #  owner   => mservices,
  #  group   => mservices,
  #  mode    => 644,
  #}->

  file { "assert docker image ${name} build path contains puppet /modules":
    path    => "${base}/${name}/modules",
    ensure  => directory,
    recurse => true,
    purge   => true,
    force   => true,
    source  => "${modules}",
    owner   => mservices,
    group   => mservices,
    links   => follow,
  }->

  file { "create dockerfile descriptor ${name} with puppet codfe ${puppet_code}":
    path    => "${base}/${name}/Dockerfile",
    ensure  => file,
    owner   => mservices,
    group   => mservices,
    mode    => 644,
    content => template("meindocker/dockerfile_${service_type}_microservice.erb"),
  }
}
