class systemd {
	
	# systemd https://wiki.debian.org/systemd#systemd_-_system_and_service_manager
	package { 'systemd':
		ensure => present,
		install_options => [ '--force-yes'],
	}
	
	package { 'systemd-sysv':
		ensure => present,
		install_options => [ '--force-yes'],
	}

}
