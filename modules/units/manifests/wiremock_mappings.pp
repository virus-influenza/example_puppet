class units::wiremock_mappings(

  $unit         = "api-cook",
  $revision     = "feature_ratamx_contract_states",
  $mappings_tag = "prod",

){

  require paths::docker::wiremock

  $files_folder = $paths::docker::wiremock::files
  $mappings_folder = $paths::docker::wiremock::mappings

  # speiseplan puppet codebase unit
  dahood::house{
    $unit:
      unit        => $unit,
      description => "json api contracts for ${mappings_tag} environment",
      revision    => $revision,
      orig_repo   => "git@bitbucket.org:Lucaretto/%UNIT%.git"
  }->

  dahood::linka{
    "wiremock mappings for ${mappings_tag} state":
      link   => '/opt/wiremock/mappings', # params
      target => "/home/mservices/repos/units/${unit}/${mappings_tag}/${mappings_folder}" # param
  }->

  # wiremock cant read sym linked files
  #dahood::linka{
  #  "wiremock files for ${mappings_tag} state":
  #    link   => '/opt/wiremock/__files', # params
  #    target => "/home/mservices/repos/units/${unit}/${mappings_tag}/${files_folder}" # param
  #}

  dahood::copy{
    "wiremock files for ${mappings_remtag} state":
      destination  => '/opt/wiremock/__files', # params
      source       => "/home/mservices/repos/units/${unit}/${mappings_tag}/${files_folder}" # param
  }

}
