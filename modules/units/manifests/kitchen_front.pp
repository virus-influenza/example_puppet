class units::kitchen_front(
  $unit         = "kitchen_front",
  $revision     = "unstable",
  $exec_command = "npm install && gulp build",
  $exec_opts    = "--env production"
){


  # speiseplan kitchen_front code base
  dahood::house{
    $unit:
      unit        => $unit,
      description => "kitchen front code base, CMS for rdbms kitchen data",
      revision    => $revision,
      orig_repo   => "git@bitbucket.org:Lucaretto/%UNIT%.git"
  }->

  file { "/$unit":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }->

  dahood::linka{
    "${unit} code base":
      link   => "/${unit}/${revision}",
      target => "/home/mservices/repos/units/${unit}" # param
  }->

  dahood::exec{
    "${unit} exec command ${exec_command} with options ${exec_opts}":
      unit         => $unit,
      exec_command => $exec_command,
      exec_opts    => $exec_opts,
      pwd          => "/${unit}/${revision}"
  }

}
