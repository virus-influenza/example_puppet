class units::portal_front(
  $unit         = "portal_front",
  $revision     = "unstable",
  $exec_command = "npm install && gulp build",
  $exec_opts    = "--env production"
){


  # speiseplan portal_front code base
  dahood::house{
    $unit:
      unit        => $unit,
      description => "portal front code base, search engine portal",
      revision    => $revision,
      orig_repo   => "git@bitbucket.org:virus-influenza/%UNIT%.git"
  } ->

  file { "/$unit":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 644,
  }->

  dahood::linka{
    "${unit} code base":
      link   => "/${unit}/${revision}",
      target => "/home/mservices/repos/units/${unit}" # param
  } # ->

  /*
    portal front end is still not ready to go live, i will just add a simple index.html inside a dist folder
  dahood::exec{
    "${unit} exec command ${exec_command} with options ${exec_opts}":
      unit         => $unit,
      exec_command => $exec_command,
      exec_opts    => $exec_opts,
      pwd          => "/${unit}/${revision}"
  }
  */
}
