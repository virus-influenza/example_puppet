class units::fabric_base(
){

# speiseplan puppet codebase unit
  dahood::house{
    'speiseplan-fabric':
      unit        => 'speiseplan-fabric',
      description => 'fabric base code',
      revision    => 'unstable',
  }->

  dahood::linka{
    'fabric tasks':
      link   => '/fabric',
      target => "/home/mservices/repos/units/speiseplan-fabric"
  }

}
