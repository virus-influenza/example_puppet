class units::puppet_base(
){

  # speiseplan puppet codebase unit
  dahood::house{
    'speiseplan-puppet':
      unit        => 'speiseplan-puppet',
      description => 'puppet base code',
      revision    => 'unstable', # huge refactor :)
  } ->

  dahood::linka{
    'puppet modules':
      link   => '/modules',
      target => "/home/mservices/repos/units/speiseplan-puppet/modules"
  }

}
