class profiles::devteam::base(

){

  # base path for mservices and units repositories
  require paths::repositories

  # microservices user
  class{ 'users::mservices' : } ->

    # default paths, /opt/packages and /opt/binaries
  class{ 'paths': } ->

    # git
  class { 'meingit': } ->

    # code tools
    # java
  class{ 'meinjava':
    package => 'openjdk-7-jdk'
  } ->
  class{ 'gradle': } ->

    # nodejs and gulp
  class{ 'nodejs': } ->

    # creates puppet repository and convenience /modules link for puppet apply
  class{ 'units::puppet_base': } ->

    # docker
  class { 'meindocker::install': } ->

    # mangodb
  class { 'meinmongo':
    bind_ip => ['127.0.0.1'],
    port    => 27017,
  } ->

    # mysql
  class { $database_param:
    bind_ip => '127.0.0.1'
  } ->

    # rabbitmq
  class{ 'meinrabbit':
    bind_ip           => '127.0.0.1',
    delete_guest_user => false
  }


  # fabric instalation managed by fabric profile 8)
  # creates fabric repositort and convenience /fabric link for fab execution
  #class{ 'units::fabric_base': } ->
  # fabric user
  #class{ 'users::fabric': }

}