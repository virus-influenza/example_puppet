class profiles::iron::rdbms(
){
  require hosts::params::all

  class { "meinmysql":
    bind_ip => $hosts::params::speiseplan_b::network_ip
  }

}
