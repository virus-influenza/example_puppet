class profiles::iron::amqp(
){

  require hosts::params::all

  class{ 'meinrabbit':

    bind_ip => $hosts::params::speiseplan_b::network_ip
  }

  rabbitmq_user { 'daneel':
    admin    => true,
    password => '_levit4!!!',
  }

  rabbitmq_user_permissions { 'daneel@/':
    configure_permission => '.*',
    read_permission      => '.*',
    write_permission     => '.*',
  }
}
