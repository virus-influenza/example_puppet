class profiles::iron::ddbms(
){
	require hosts::params::all

	class {'meinmongo':
		port    => 27017,
		bind_ip => [$hosts::params::speiseplan_b::network_ip]
	}

	#
  # https://tickets.puppetlabs.com/browse/MODULES-534
  #mongodb::db { 'speiseplan_mgimmy':
  #  user          => 'daneel',
  #  password_hash => 'f88b5d3a2cd44200b45acf27939afc8b', #this means pass1
  #}
}
