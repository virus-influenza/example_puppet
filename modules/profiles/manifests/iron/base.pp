class profiles::iron::base(
){

  # users
  class{ 'users': } ->

  # paths
  class{ 'paths::repositories': } ->

  # puppet base code unit with /modules
  class { 'units::puppet_base': } ->

  # puppet firewall module
  class{ 'meinfirewall': } ->

  # locales
  class { 'meinlocales::base': }

}
