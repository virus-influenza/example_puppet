class profiles::systemctl::search_api(
){

  firewall { '100 allow access to mfoodpoint-search-api microservice':
    port   => 2000,
    proto  => 'tcp',
    action => 'accept',
  }

# search api using data access micro service
  kidstoschool::service {
    'mfoodpoint-search-api':
      mservice     => "mfoodpoint-search-api",
      service_type => "node",
      bin_opts     => "",
      service_opts => "",
  }
}
