class profiles::systemctl::broker(
){

  # TODO: refactor to use the common interface microservice::systemd
  kidstoschool::service {
    'mwolf':
      mservice     => 'mwolf',
      description  => 'amqp broker configuration microservice',
      service_type => 'java',
      bin_opts     => '-jar -Xmx250m',
      service_opts => '--spring.profiles.active=prod',
  }
}
