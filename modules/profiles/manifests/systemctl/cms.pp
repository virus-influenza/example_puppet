class profiles::systemctl::cms(
){

  # TODO: refactor to use common interface microservice::systemd
  kidstoschool::service {
    'mfoodpoint':
      mservice     => 'mfoodpoint',
      description  => 'rdbms food point microservice',
      service_type => 'java',
      bin_opts     => '-jar -Xmx250m',
      service_opts => '--spring.profiles.active=prod';
  }

}
