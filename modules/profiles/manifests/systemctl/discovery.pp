class profiles::systemctl::discovery(
){

	kidstoschool::service {
        'markimedes':
                mservice     => 'markimedes',
                description  => 'spring cloud netflix eureka service registry server',
                service_type => 'java',
                bin_opts     => '-jar -Xmx150m',
                service_opts => 'spring.profiles.active=prod';
	}

}
