class profiles::systemctl::search(
){

  firewall { '100 allow lo access to rdbms mfoodpoint microservice':
    port   => 8081,
    source => '127.0.0.1',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '100 allow 10.133.182.133 access to rdbms mfoodpoint microservice':
    port   => 8081,
    source => '10.133.182.133',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '100 allow 10.133.222.67 access to rdbms mfoodpoint microservice':
    port   => 8081,
    source => '10.133.222.67',
    proto  => 'tcp',
    action => 'accept',
  }

  # TODO: refactor to user common interface microservice::systemd
  # data access micro service for mangodb searches
  kidstoschool::service {
    'mgimmy':
      mservice     => 'mgimmy',
      description  => 'mangodb searchable food point microservice',
      service_type => 'java',
      bin_opts     => '-jar -Xmx250m',
      service_opts => '--spring.profiles.active=prod',
  }
}
