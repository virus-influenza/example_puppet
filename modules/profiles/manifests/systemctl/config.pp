class profiles::systemctl::config(
){
	kidstoschool::service {
        'mconfig':
                mservice     => 'mconfig',
                description  => 'spring cloud config server',
                service_type => 'java',
                bin_opts     => '-jar -Xmx150m',
                service_opts => '--spring.profiles.active=prod';
	}
}
