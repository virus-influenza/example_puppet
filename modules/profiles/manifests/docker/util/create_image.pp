# interface to create docker images from dockerfiles for micro services
define profiles::docker::util::create_image(
  # docker image
  $mservice,
  $flavour,
  $registry,
  $user,
  $port,
  $service_type,
  $puppet_code,

  # running service
  $bin_opts,
  $service_opts,
){

  # base prerequisites for building docker images
  require profiles::docker::base::host

  # create puppet enabled Dockerfile descriptor
  # template meindocker/templates/dockerfile_$service_type.erb
  #	/home/mservices/docker/images/myservice/
  #		.Dockerfile
  #		.modules/
  meindocker::dockerfile::mservice{ "prepares a docker build folder for ${mservice}":
    name          => $mservice,
    flavour       => $flavour,
    service_type  => $service_type,
    bin_opts      => $bin_opts,
    service_opts  => $service_opts,
    port          => $port,
    puppet_code   => $puppet_code,
  } ->

  ## builds the image prepared by microservice::dockerize
  ## tags the image created to enable private registry push
  ## push the image to private registry
  meindocker::image::build { "${registry}:${port}/${user}/${mservice}:${flavour}":
    registry        => $registry,
    image           => $mservice,
    user            => $user,
    flavour         => ":$flavour",
    dockerfile_dir  => "/home/mservices/docker/images/${mservice}/", # param
  }

}