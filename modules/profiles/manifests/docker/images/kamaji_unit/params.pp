class profiles::docker::images::kamaji_unit::params(
){
  # docker image
  $mservice     = 'kamaji-unit'
  $image_tag    = 'unstable'
  $port         = '8080'
  $expose       = '80'
  $service_type = 'node'
  $service_user = 'fabric' # due to fab enabled app

  # registry
  $registry     = 'speiseplan.menu:5000'
  $user         = 'virus'

  # build service
  $build        = 'npm install && gulp build'
  $build_opts   = ''

  # build box
  $puppet_code  = [
    # packages the mservice inside the docker image
    "include roles::images::kamaji_unit_box",
  ]

  require hosts::params::speiseplan_a
  $hostname     = $hosts::params::speiseplan_a::kamaji

  # running service
  # bin options in node as environment variables
  $bin_opts     = ['NODE_ENV=PROD']
  $service_opts = ''
}