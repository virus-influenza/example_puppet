class profiles::docker::images::kamaji_unit::build(
){

  require profiles::docker::images::kamaji_unit::params
  require profiles::docker::images::registry::params

  # docker image
  $mservice     = $profiles::docker::images::kamaji_unit::params::mservice
  $image_tag    = $profiles::docker::images::kamaji_unit::params::image_tag
  $port         = $profiles::docker::images::kamaji_unit::params::port
  $service_type = $profiles::docker::images::kamaji_unit::params::service_type
  $puppet_code  = $profiles::docker::images::kamaji_unit::params::puppet_code

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # running service
  $bin_opts     = $profiles::docker::images::kamaji_unit::params::bin_opts
  $service_opts = $profiles::docker::images::kamaji_unit::params::service_opts


  # create Dockerfile, create Docker Image
  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${image_tag}" :

      mservice     => $mservice,
      image_tag    => $image_tag,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }

}
