class profiles::docker::images::kamaji_unit::package(
){

  require profiles::docker::images::kamaji_unit::params

  $mservice   = $profiles::docker::images::kamaji_unit::params::mservice
  $revision   = $profiles::docker::images::kamaji_unit::params::image_tag

  $build      = $profiles::docker::images::kamaji_unit::params::build
  $build_opts = $profiles::docker::images::kamaji_unit::params::build_opts

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to  => "/apps",
    # require     => Class['Profiles::Docker::Base::Image'] # seems to not work properly
  }



}