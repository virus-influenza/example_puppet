class profiles::docker::images::node::build(
){
  require profiles::docker::images::registry::params

  # docker image
  $image        = hiera("node_image")
  $flavour      = hiera("node_flavour")
  $from         = hiera("node_from")

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

# create Dockerfile descriptor
#
#	meindocker::dockerfile::node_base
# template meindocker/templates/dockerfile_node_base.erb
#	/home/mservices/docker/images/node_base
#		.Dockerfile
  meindocker::dockerfile::service_base{"creates a docker build folder for ${image}":
    image       => $image,
    flavour     => $flavour,
    port        => false,
    from        => $from,
    puppet_code => [ 'include profiles::docker::images::node::install' ],
  }->

  ## builds the image
  ## tags the image created to enable private registry push
  ## push the image to private registry
  meindocker::image::build { "${registry}/${user}/${image}:${flavour}":
    registry        => $registry,
    image           => $image,
    user            => $user,
    flavour => ":${flavour}",
    dockerfile_dir  => "/home/mservices/docker/images/${image}/",
  }

}