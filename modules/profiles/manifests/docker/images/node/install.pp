class profiles::docker::images::node::install(
){

  # hiera config
  $nodejs_version = hiera("node_nodejs_version")
  notice(" MAY I HAVE YOUR ATENTION PLEASE ")
  notice($nodejs_version)

  # include docker base image, paths, git, java, gradle
  class{ "profiles::docker::base::image": }->

    # build tools
    # node
  class{ "nodejs::install":
    version => $nodejs_version,
    source  => "http://nodejs.org/dist/latest-v5.x/"
  }->

  # gulp
  nodejs::package{ 'gulp':
    name    => 'gulp',
    options => '--global',
    node_version => $nodejs_version
  }
}