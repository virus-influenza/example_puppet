class profiles::docker::images::puppet::build(
){

  require profiles::docker::images::registry::params

  # docker image
  $image        = hiera("puppet_image")
  $image_tag    = hiera("puppet_image_tag")
  $version      = hiera("puppet_version")
  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # create Dockerfile descriptor
  #
  #	meindocker::dockerfile::puppet_base
  # template meindocker/templates/dockerfile_puppet_base.erb
  #	/home/mservices/docker/images/puppet_base
  #		.Dockerfile
  meindocker::dockerfile::puppet_base{"creates a docker build folder for ${image}":
    image   => $image,
    version => $version
  }->

  ## builds the image
  ## tags the image created to enable private registry push
  ## push the image to private registry
  meindocker::image::build {"${registry}/${user}/${image}:${image_tag}":
    registry        => $registry,
    image           => $image,
    user            => $user,
    flavour         => ":${image_tag}",
    dockerfile_dir  => "/home/mservices/docker/images/${image}/",
  }

}