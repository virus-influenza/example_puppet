class profiles::docker::images::artifactory::service(
){

  # mnetwork hosts
  require hosts::params::all
  # docker registry
  require profiles::docker::images::registry::params

  # hiera
  $image        = hiera("artifactory_image")
  $name         = hiera("artifactory_name")
  $flavour      = hiera("artifactory_flavour")
  $port         = hiera("artifactory_port")
  $expose       = hiera("artifactory_expose")

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  ## installs latest image in host
  docker::image { "${image}":
    image_tag  => "${flavour}",
    ensure     => latest,
  } ->

    # problem to run this image using puppet module, ni tan mal
  exec{ "docker rm ${name} >/dev/null 2>&1":
    command => "docker rm ${name} >/dev/null 2>&1",
    path    => ["/usr/bin/"],
  } ->

    # just fucking exec this please docker run -d -p 5004:8081 --name nexus sonatype/nexus:oss
  exec { "docker run -d -p ${expose}:${port} --name ${name} ${image}:${flavour}":
    command => "docker run -d -p ${expose}:${port} --name ${name} ${image}:${flavour}",
    path    => ["/usr/bin/"],
    user    => root,
  }
}
