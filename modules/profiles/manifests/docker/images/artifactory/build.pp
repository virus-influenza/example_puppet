class profiles::docker::images::artifactory::build(
){
  require profiles::docker::images::registry::params

  # docker image
  # hiera config
  $image        = hiera("artifactory_image")
  $flavour      = hiera("artifactory_flavour")
  $from         = hiera("artifactory_from")

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # create Dockerfile descriptor
  #
  #	meindocker::dockerfile::artifactory
  # template meindocker/templates/dockerfile_artifactory.erb
  #	/home/mservices/docker/images/artifactory
  #		.Dockerfile
  meindocker::dockerfile::artifactory{ "creates a docker build folder for ${image}":
    image => $image,
    from => $from,
  }->

    ## builds the image
    ## tags the image created to enable private registry push
    ## push the image to private registry
  meindocker::image::build { "${registry}/${user}/${image}:${flavour}":
    registry        => $registry,
    image           => $image,
    user            => $user,
    flavour         => ":${flavour}",
    dockerfile_dir  => "/home/mservices/docker/images/${image}/",
  }

}