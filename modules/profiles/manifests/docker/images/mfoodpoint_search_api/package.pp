class profiles::docker::images::mfoodpoint_search_api::package(
){

  require profiles::docker::images::mfoodpoint_search_api::params

  $mservice   = $profiles::docker::images::mfoodpoint_search_api::params::mservice
  $revision   = $profiles::docker::images::mfoodpoint_search_api::params::image_tag

  $build      = $profiles::docker::images::mfoodpoint_search_api::params::build
  $build_opts = $profiles::docker::images::mfoodpoint_search_api::params::build_opts

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to  => "/apps",
    # require     => Class['Profiles::Docker::Base::Image'] # seems to not work properly
  }



}