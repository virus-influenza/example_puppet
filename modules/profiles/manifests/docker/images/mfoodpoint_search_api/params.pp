class profiles::docker::images::mfoodpoint_search_api::params(
){
  # docker image
  $mservice     = 'mfoodpoint-search-api'
  $image_tag    = 'unstable'
  $port         = '8080'
  $expose       = '10301'
  $service_type = 'node'

  # registry
  $registry     = 'speiseplan.menu:5000'
  $user         = 'virus'

  # build service
  $build        = 'npm install && gulp build'
  $build_opts   = ''

  # build box
  $puppet_code  = [
    # packages the mservice inside the docker image
    "include profiles::docker::images::mfoodpoint_search_apit::package",
  ]

  # running service
  # bin options in node as environment variables
  $bin_opts     = ['NODE_ENV=prod']
  $service_opts = ''
}