class profiles::docker::images::mfoodpoint_search_api::service(
){
  # mnetwork hosts
  require hosts::params::all
  require microservice::params::node
  require profiles::docker::images::mfoodpoint_search_api::params
  require profiles::docker::images::registry::params

  $mservice     = $profiles::docker::images::mfoodpoint_search_api::params::mservice
  $image_tag    = $profiles::docker::images::mfoodpoint_search_api::params::image_tag
  $port         = $profiles::docker::images::mfoodpoint_search_api::params::port
  $expose       = $profiles::docker::images::mfoodpoint_search_api::params::expose

  $bin          = $microservice::params::node::bin_exec
  $bin_opts     = $profiles::docker::images::mfoodpoint_search_api::params::bin_opts
  $service_opts = $profiles::docker::images::mfoodpoint_search_api::params::service_opts

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # node bin opts are used as environment variables in the image:run
  $command      = "${bin} ${mservice}-${image_tag}.js ${service_opts}"

  ## installs latest image in host
	docker::image { "${registry}/${user}/${mservice}":
    image_tag  => $image_tag,
    ensure     => latest,
	} ->

  ## runs image in host
	docker::run { "${mservice}:${image_tag}":
		image            => "${registry}/${user}/${mservice}:${image_tag}",
    pull_on_start    => true,
    memory_limit     => '256m',
    ports            => ["${expose}:${port}"],
    restart_service  => true,
    force_restart    => true,
    command          => $command,
    env              => $bin_opts,
    hostentries      => [
      "${hosts::params::speiseplan_a::hostname}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_b::hostname}:${hosts::params::speiseplan_b::internal_ip}",

      # links
      # _self
      "${hosts::params::speiseplan_a::api}:${hosts::params::speiseplan_a::internal_ip}",
      # backend
      "${hosts::params::speiseplan_a::mgimmy}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_a::mfoodpoint}:${hosts::params::speiseplan_a::internal_ip}"
    ],
	}
}
