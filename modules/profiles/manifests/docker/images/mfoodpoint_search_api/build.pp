class profiles::docker::images::mfoodpoint_search_api::build(
){

  require profiles::docker::images::mfoodpoint_search_api::params
  require profiles::docker::images::registry::params

  # docker image
  $mservice     = $profiles::docker::images::mfoodpoint_search_api::params::mservice
  $image_tag    = $profiles::docker::images::mfoodpoint_search_api::params::image_tag
  $port         = $profiles::docker::images::mfoodpoint_search_api::params::port
  $service_type = $profiles::docker::images::mfoodpoint_search_api::params::service_type
  $puppet_code  = $profiles::docker::images::mfoodpoint_search_api::params::puppet_code

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # running service
  $bin_opts     = $profiles::docker::images::mfoodpoint_search_api::params::bin_opts
  $service_opts = $profiles::docker::images::mfoodpoint_search_api::params::service_opts

  # tricky, mservices use - and puppet _ :$
  $sanitized_mservice = regsubst($mservice,'-','_','G')

  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${image_tag}" :

      mservice     => $mservice,
      image_tag    => $image_tag,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }

}
