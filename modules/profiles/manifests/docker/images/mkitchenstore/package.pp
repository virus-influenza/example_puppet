class profiles::docker::images::mkitchenstore::package(
  $mservice,
  $revision,

  $build,
  $build_opts,
){

  # create and install in house build dependency, until maven artifactory is working
  microservice::util::build { "mkitchencontext for ${mservice}":
    mservice    => "mkitchencontext",
    revision    => $revision,
    build       => "gradle clean install",
    build_opts  => "-x test",
    deliver_to  => "/tmp/mkitchencontext-mkitchenstore.jar.noop"
  }->

    # create and install in house build dependency, until maven artifactory is working
  microservice::util::build { "anachronistic for ${mservice}":
    mservice    => "anachronistic",
    revision    => $revision,
    build       => "gradle clean install",
    build_opts  => "-x test",
    deliver_to  => "/tmp/anachronistic-mkitchenstore.jar.noop"
  }->

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to  => "/home/mservices/docker/images/${mservice}/"
  }

}