class profiles::docker::images::mkitchenstore::build(
  # hiera configured parameters
  # docker image
  $mservice,
  $flavour,
  $port,
  $service_type,
  $puppet_code,
  # docker registry configuration
  $registry,
  $user,
  # running service
  $bin_opts,
  $service_opts

){

  # creates and publish distributed mservice package while we dont have a CI server
  class{ "profiles::docker::images::mkitchenstore::package" : }->

  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${flavour}" :

      mservice     => $mservice,
      flavour      => $flavour,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }
}
