class profiles::docker::images::mauthentication::package(
  $mservice,
  $revision,

  $build,
  $build_opts,
){

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    origin     => "git@bitbucket.org:naknatxo/mauthentication.git",
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to  => "/home/mservices/docker/images/${mservice}/"
  }

}