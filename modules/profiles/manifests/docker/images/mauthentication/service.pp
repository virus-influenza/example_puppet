class profiles::docker::images::mauthentication::service(
  # docker image
  $mservice,
  $flavour,
  $port,
  $expose,
  $volumes,
  # running service
  $bin,
  $bin_opts,
  $service_opts,
  # docker registry
  $registry,
  $user,
){

  # parameters
  require hosts::params::all

  # service command
  $command      = "${bin} ${bin_opts} -jar ${mservice}-${flavour}.jar ${service_opts}"

  ## installs latest image in host
  docker::image { "${registry}/${user}/${mservice}":
    image_tag  => $flavour,
    ensure     => latest,
  } ->

  ## runs image in host
  docker::run { "${mservice}:${flavour}":
    image            => "${registry}/${user}/${mservice}:${flavour}",
    net              => "bridge",
    pull_on_start    => true,
    memory_limit     => '256m',
    ports            => ["${expose}:${port}"],
    restart_service  => true,
    force_restart    => true,
    command          => $command,
    volumes => $volumes,
    hostentries      => [
      "${hosts::params::speiseplan_a::hostname}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_b::hostname}:${hosts::params::speiseplan_b::internal_ip}",
      # links
      # _self
      "${hosts::params::speiseplan_a::mauthentication}:${hosts::params::speiseplan_a::internal_ip}",
      # distributed config
      "${hosts::params::speiseplan_a::mconfig}:${hosts::params::speiseplan_a::internal_ip}",
      # eureka registry server
      "${hosts::params::speiseplan_a::markimedes}:${hosts::params::speiseplan_a::internal_ip}",
      # cms api
      "${hosts::params::speiseplan_a::mkitchenstore}:${hosts::params::speiseplan_a::internal_ip}",
      # cms api
      "${hosts::params::speiseplan_a::mgimmy}:${hosts::params::speiseplan_a::internal_ip}",
      # services
      "${hosts::params::speiseplan_b::mysql}:${hosts::params::speiseplan_b::network_ip}",
      "${hosts::params::speiseplan_b::rabbitmq}:${hosts::params::speiseplan_b::network_ip}",
    ],
  }
}