class profiles::docker::images::wiremock::params(
){
  $image        = "wiremock"
  $image_tag    = "stable"

  $port         = 8080
  $expose       = 5001

  $volumes      = [
    "/opt/wiremock/__files:/wiremock/__files",
    "/opt/wiremock/mappings:/wiremock/mappings",
  ]

  $service_opts = "--port ${port} --verbose"
}