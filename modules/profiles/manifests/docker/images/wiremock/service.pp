class profiles::docker::images::wiremock::service(
){

  # mnetwork hosts
  require hosts::params::all
  require microservice::params::java
  require profiles::docker::images::wiremock::params
  require profiles::docker::images::registry::params

  $mservice     = $profiles::docker::images::wiremock::params::image
  $image_tag    = $profiles::docker::images::wiremock::params::image_tag
  $port         = $profiles::docker::images::wiremock::params::port
  $expose       = $profiles::docker::images::wiremock::params::expose
  $volumes      = $profiles::docker::images::wiremock::params::volumes

  $bin          = $microservice::params::java::bin_exec
  $service_opts = $profiles::docker::images::wiremock::params::service_opts

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # java -jar wiremock-1.57-standalone.jar
  # java -jar wiremock-stable.jar
  $command      = "${bin} -jar ${mservice}-${image_tag}.jar $service_opts"

## installs latest image in host
  docker::image { "${registry}/${user}/${mservice}":
    image_tag  => $image_tag,
    ensure     => latest,
  } ->

  ## runs image in host
  docker::run { "${mservice}:${image_tag}":
    image            => "${registry}/${user}/${mservice}:${image_tag}",
    pull_on_start    => true,
    memory_limit     => '128m',
    #ports            => ["${hosts::params::speiseplan_a::internal_ip}:${expose}:${port}"],
    ports            => ["${expose}:${port}"],
    restart_service  => true,
    force_restart    => true,
    command          => $command,
    volumes          => $volumes,
    hostentries      => [
      "${hosts::params::speiseplan_a::hostname}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_b::hostname}:${hosts::params::speiseplan_b::internal_ip}",
    ],
  }

}
