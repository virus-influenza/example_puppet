class profiles::docker::images::wiremock::build(
){
  require profiles::docker::images::wiremock::params
  require profiles::docker::images::registry::params

# docker image
  $image        = $profiles::docker::images::wiremock::params::image
  $image_tag    = $profiles::docker::images::wiremock::params::image_tag
  $port         = $profiles::docker::images::wiremock::params::port

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # create Dockerfile descriptor
  meindocker::dockerfile::service_base{"creates a docker build folder for ${image}":
    image       => $image,
    puppet_code => [ 'include profiles::docker::base::wiremock' ],
    from        => "java_base:unstable", # debian testing, puppet testing, java 8
    template    => "dockerfile_wiremock.erb",
    port        => $port,
  }->

  ## builds the image
  ## tags the image created to enable private registry push
  ## push the image to private registry
  meindocker::image::build {"${registry}/${user}/${image}:${image_tag}":
    registry        => $registry,
    image           => $image,
    user            => $user,
    image_tag       => ":${image_tag}",
    dockerfile_dir  => "/home/mservices/docker/images/${image}/",
  }

}