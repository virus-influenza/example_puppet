class profiles::docker::images::mconfig::build(
){

  require profiles::docker::images::mconfig::params
  require profiles::docker::images::registry::params

# docker image
  $mservice     = $profiles::docker::images::mconfig::params::mservice
  $flavour      = $profiles::docker::images::mconfig::params::flavour
  $port         = $profiles::docker::images::mconfig::params::port
  $service_type = $profiles::docker::images::mconfig::params::service_type
  $puppet_code  = $profiles::docker::images::mconfig::params::puppet_code

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

# running service
  $bin_opts     = $profiles::docker::images::mconfig::params::bin_opts
  $service_opts = $profiles::docker::images::mconfig::params::service_opts

  # creates and publish distributed mservice package while we dont have a CI server
  class{ "profiles::docker::images::mconfig::package" : }->

  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${flavour}" :

      mservice     => $mservice,
      flavour      => $flavour,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }
}
