class profiles::docker::images::mconfig::package(
){

  require profiles::docker::images::mconfig::params

  $mservice   = $profiles::docker::images::mconfig::params::mservice
  $revision   = $profiles::docker::images::mconfig::params::flavour

  $build      = $profiles::docker::images::mconfig::params::build
  $build_opts = $profiles::docker::images::mconfig::params::build_opts

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to => "/home/mservices/docker/images/${mservice}/"
  }

}