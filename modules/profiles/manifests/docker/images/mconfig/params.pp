class profiles::docker::images::mconfig::params(
){
  # docker image
  $mservice     = 'mconfig'
  $flavour      = 'unstable'
  $port         = '8080'
  $expose       = '10000'
  $service_type = 'java'

  # registry
  $registry     = 'speiseplan.menu:5000'
  $user         = 'virus'

  # service
  # build service
  $build        = 'gradle build'
  $build_opts   = '-x test'

  # build box
  $puppet_code  = []
  $volumes      = [ "/var/log/:/var/log/mservices/" ]

  # running service
  $bin_opts     = '-Xmx128m'
  $service_opts = '--spring.profiles.active=prod'
  $service_user = 'mservices'
}