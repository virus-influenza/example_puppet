class profiles::docker::images::java::build(
){

  require profiles::docker::images::registry::params

  # hiera config
  $image        = hiera("java_image")
  $flavour      = hiera("java_flavour")
  $from         = hiera("java_from")

  # registry parameters
  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # create Dockerfile descriptor
  #
  #	meindocker::dockerfile::java_base
  # template meindocker/templates/dockerfile_java_base.erb
  #	/home/mservices/docker/images/node_base
  #		.Dockerfile
  meindocker::dockerfile::service_base{"creates a docker build folder for ${image}":
    image       => $image,
    flavour     => $flavour,
    port        => false,
    from        => $from,
    puppet_code => [ 'include profiles::docker::images::java::install' ],
  }->

  ## builds the image
  ## tags the image created to enable private registry push
  ## push the image to private registry
  meindocker::image::build { "${registry}/${user}/${image}:${flavour}":
    registry        => $registry,
    image           => $image,
    user            => $user,
    flavour         => ":${flavour}",
    dockerfile_dir  => "/home/mservices/docker/images/${image}/",
  }

}