class profiles::docker::images::java::install(
){

  # hiera config
  $jre_package    = hiera("java_jre_package")
  $gradle_version = hiera("java_gradle_version")

  # include docker base image, paths, git, java, gradle
  class{ "profiles::docker::base::image": }->

    # build tools
    # java
  class{ 'meinjava':
    package => $jre_package
  } ->
  class{ 'gradle':
    version => $gradle_version
  }
}