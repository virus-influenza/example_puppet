class profiles::docker::images::registry::service(
){

  require profiles::docker::images::registry::params

  $host   = $profiles::docker::images::registry::params::host
  $port   = $profiles::docker::images::registry::params::port
  $expose = $profiles::docker::images::registry::params::port

  ## included in host base
  # class { 'paths::docker::base': } ->
  class { 'paths::docker::registry': } ->

	class { 'meindocker::certificate':
		host => $host,
		port => $port,
	} ~>

  Service['docker'] ~>

	# https://github.com/docker/distribution/blob/master/docs/deploying.md
	docker::run { 'registry':
		image            => 'registry:2',
    net              => "bridge",
		extra_parameters => ['--restart=always'],
		ports            => ["${expose}:${port}"],
		env              => [
      'REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY=/var/lib/registry',
      'REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt',
      'REGISTRY_HTTP_TLS_KEY=/certs/domain.key'
    ],
		volumes          => [
      '/opt/docker/registry/:/var/lib/registry',
      '/opt/docker/certs:/certs'
    ],
	}
}
