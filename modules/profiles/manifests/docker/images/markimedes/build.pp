class profiles::docker::images::markimedes::build(
){

  require profiles::docker::images::markimedes::params
  require profiles::docker::images::registry::params

  # docker image
  $mservice     = $profiles::docker::images::markimedes::params::mservice
  $flavour    = $profiles::docker::images::markimedes::params::flavour
  $port         = $profiles::docker::images::markimedes::params::port
  $service_type = $profiles::docker::images::markimedes::params::service_type
  $puppet_code  = $profiles::docker::images::markimedes::params::puppet_code

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # running service
  $bin_opts     = $profiles::docker::images::markimedes::params::bin_opts
  $service_opts = $profiles::docker::images::markimedes::params::service_opts

  # creates and publish distributed mservice package while we dont have a CI server
  class{ "profiles::docker::images::markimedes::package" : }->

  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${flavour}" :

      mservice     => $mservice,
      flavour    => $flavour,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }
}
