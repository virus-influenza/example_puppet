class profiles::docker::images::markimedes::params(
){
  # docker image
  $mservice     = 'markimedes'
  $flavour    = 'unstable'
  $port         = '8080'
  $expose       = '10010'
  $service_type = 'java'

  # registry
  $registry     = 'speiseplan.menu:5000'
  $user         = 'virus'

  # service
  # build service
  $build        = 'gradle build'
  $build_opts   = ''

  # build box
  $puppet_code  = [
  # packages the mservice inside the docker image
    "include profiles::docker::images::markimedes::package",
  ]

  # running service
  $bin_opts     = '-Xmx128m'
  $service_user = 'mservices'

  $service_opts = "--spring.profiles.active=prod \
  --spring.application.name=${mservice} \
  --spring.cloud.config.label=${flavour} \
  --eureka.instance.nonSecurePort=${expose} \
  --eureka.instance.appname=${mservice}-${flavour} \
  --eureka.instance.hostname=${mservice}.speiseplan"
}