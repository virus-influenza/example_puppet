class profiles::docker::images::markimedes::package(
){

  require profiles::docker::images::markimedes::params

  $mservice   = $profiles::docker::images::markimedes::params::mservice
  $revision   = $profiles::docker::images::markimedes::params::flavour

  $build      = $profiles::docker::images::markimedes::params::build
  $build_opts = $profiles::docker::images::markimedes::params::build_opts

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to => "/home/mservices/docker/images/${mservice}/"
  }

}