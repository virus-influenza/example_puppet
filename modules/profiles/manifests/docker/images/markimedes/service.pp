class profiles::docker::images::markimedes::service(
){

  # mnetwork hosts
  require hosts::params::all
  require microservice::params::java
  require profiles::docker::images::markimedes::params
  require profiles::docker::images::registry::params

  $mservice     = $profiles::docker::images::markimedes::params::mservice
  $flavour    = $profiles::docker::images::markimedes::params::flavour
  $port         = $profiles::docker::images::markimedes::params::port
  $expose       = $profiles::docker::images::markimedes::params::expose

  $bin          = $microservice::params::java::bin_exec
  $bin_opts     = $profiles::docker::images::markimedes::params::bin_opts
  $service_opts = $profiles::docker::images::markimedes::params::service_opts
  $service_user = $profiles::docker::images::markimedes::params::service_user

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  $command      = "${bin} ${bin_opts} -jar ${mservice}-${flavour}.jar ${service_opts}"

## installs latest image in host
  docker::image { "${registry}/${user}/${mservice}":
    image_tag  => $flavour,
    ensure     => latest,
  } ->

  ## runs image in host
  docker::run { "${mservice}:${flavour}":
    image            => "${registry}/${user}/${mservice}:${flavour}",
    pull_on_start    => true,
    net              => "bridge",
    memory_limit     => '128m',
    #ports            => ["${hosts::params::speiseplan_a::internal_ip}:${expose}:${port}"],
    ports            => ["${expose}:${port}"],
    restart_service  => true,
    force_restart    => true,
    command          => $command,
    ## markimedes runs with mservice user to gain read access to git repositories
    username         => $service_user,
    hostentries      => [
      "${hosts::params::speiseplan_a::hostname}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_b::hostname}:${hosts::params::speiseplan_b::internal_ip}",
      # links
      # _self
      "${hosts::params::speiseplan_a::markimedes}:${hosts::params::speiseplan_a::internal_ip}",
      # services
      "${hosts::params::speiseplan_a::api}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_a::mgimmy}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_a::mkitchenstore}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_a::mconfig}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_a::mwolf}:${hosts::params::speiseplan_a::internal_ip}",
    ],
  }
}