class profiles::docker::images::microwave::service(
  $mservice,
  $flavour,
  $port,
  $expose,
  $volumes,
  $bin,
  $bin_opts,
  $service_opts,
  $registry,
  $user,
){
  # mnetwork hosts
  require hosts::params::all

  $command      = "${bin} ${bin_opts} ${mservice}.js ${service_opts}"

  ## installs latest image in host
  docker::image { "${registry}/${user}/${mservice}":
    image_tag  => $flavour,
    ensure     => latest,
  } ->

    ## runs image in host
  docker::run { "${mservice}:${flavour}":
    image            => "${registry}/${user}/${mservice}:${flavour}",
    net              => "bridge",
    pull_on_start    => true,
    memory_limit     => '256m',
    ports            => ["${expose}:${port}"],
    restart_service  => true,
    force_restart    => true,
    command          => $command,
    volumes          => [
      "/var/log:/var/log",
      "/kitchen.foodpoint.menu/unstable/dist:/apps/microwave/kitchen_front",
      "/portal_front/unstable/dist:/apps/microwave/portal_front"
    ],
    hostentries      => [
      "${hosts::params::speiseplan_a::hostname}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_b::hostname}:${hosts::params::speiseplan_b::internal_ip}",
      # links
      # _self
      "${hosts::params::speiseplan_a::microwave}:${hosts::params::speiseplan_a::internal_ip}",
      # distributed config
      "${hosts::params::speiseplan_a::mconfig}:${hosts::params::speiseplan_a::internal_ip}",
      # eureka service registry
      "${hosts::params::speiseplan_a::markimedes}:${hosts::params::speiseplan_a::internal_ip}",
      # mkitchenstore for data sync
      "${hosts::params::speiseplan_a::mkitchenstore}:${hosts::params::speiseplan_a::internal_ip}",
      # mgimmy just in case
      "${hosts::params::speiseplan_a::mkitchenstore}:${hosts::params::speiseplan_a::internal_ip}",
      # services
      "${hosts::params::speiseplan_b::mongodb}:${hosts::params::speiseplan_b::network_ip}",
      "${hosts::params::speiseplan_b::rabbitmq}:${hosts::params::speiseplan_b::network_ip}",
    ],
  }
}