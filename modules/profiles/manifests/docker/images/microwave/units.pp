class profiles::docker::images::microwave::units(
  $kitchen_front_unit,
  $kitchen_front_flavour,
  $kitchen_front_exec_command,
  $kitchen_front_exec_opts,

  $portal_front_unit,
  $portal_front_flavour,
  $portal_front_exec_command,
  $portal_front_exec_opts,
){

  class{ "units::kitchen_front":
    unit         => $kitchen_front_unit,
    revision     => $kitchen_front_flavour,
    exec_command => $kitchen_front_exec_command,
    exec_opts    => $kitchen_front_exec_opts
  }->

  class{ "units::portal_front":
    unit         => $portal_front_unit,
    revision     => $portal_front_flavour,
    exec_command => $portal_front_exec_command,
    exec_opts    => $portal_front_exec_opts
  }
}