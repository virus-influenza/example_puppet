class profiles::docker::images::microwave::package(
  $mservice,
  $flavour,
  $service_type,
  $build,
  $build_opts,
){

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $flavour,
    service_type => $service_type,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to  => "/home/mservices/docker/images/${mservice}/dist"
  }
}