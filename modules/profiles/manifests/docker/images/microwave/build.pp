class profiles::docker::images::microwave::build(
  # docker
  $user,
  $mservice,
  $flavour,
  $port,
  $service_type,
  $puppet_code,

  # service
  $bin,
  $bin_opts,
  $service_opts,
  $registry = 'speiseplan.menu:5000',
){

  # creates and publish distributed microwave
  class{ "profiles::docker::images::microwave::package" : }->

    # gets dependent units for microwave:
    # kitchen_front and portal_front single page js react projects
    #
    # for each unit:
    # - clone repository
    # - assert revision
    # - link to /unit
    # - execute build
  class{ "profiles::docker::images::microwave::units" : }->

  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${flavour}" :

      mservice     => $mservice,
      flavour      => $flavour,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }
}
