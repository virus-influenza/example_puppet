class profiles::docker::images::mwolf::build(
){

  require profiles::docker::images::mwolf::params
  require profiles::docker::images::registry::params

  # docker image
  $mservice     = $profiles::docker::images::mwolf::params::mservice
  $image_tag    = $profiles::docker::images::mwolf::params::image_tag
  $port         = $profiles::docker::images::mwolf::params::port
  $service_type = $profiles::docker::images::mwolf::params::service_type
  $puppet_code  = $profiles::docker::images::mwolf::params::puppet_code

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  # running service
  $bin_opts     = $profiles::docker::images::mwolf::params::bin_opts
  $service_opts = $profiles::docker::images::mwolf::params::service_opts

  profiles::docker::util::create_image{
    "${registry}/${user}/${mservice}:${image_tag}" :

      mservice     => $mservice,
      image_tag    => $image_tag,
      registry     => $registry,
      user         => $user,
      port         => $port,
      service_type => $service_type,
      puppet_code  => $puppet_code,
      bin_opts     => $bin_opts,
      service_opts => $service_opts,
  }
}
