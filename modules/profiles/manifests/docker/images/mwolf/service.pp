class profiles::docker::images::mwolf::service(
){

  # mnetwork hosts
  require hosts::params::all
  require microservice::params::java
  require profiles::docker::images::mwolf::params
  require profiles::docker::images::registry::params

  $mservice     = $profiles::docker::images::mwolf::params::mservice
  $image_tag    = $profiles::docker::images::mwolf::params::image_tag
  $port         = $profiles::docker::images::mwolf::params::port
  $expose       = $profiles::docker::images::mwolf::params::expose

  $bin          = $microservice::params::java::bin_exec
  $bin_opts     = $profiles::docker::images::mwolf::params::bin_opts
  $service_opts = $profiles::docker::images::mwolf::params::service_opts
  $service_user = $profiles::docker::images::mwolf::params::service_user

  $registry     = $profiles::docker::images::registry::params::host_port
  $user         = $profiles::docker::images::registry::params::user

  $command      = "${bin} ${bin_opts} -jar ${mservice}-${image_tag}.jar ${service_opts}"

## installs latest image in host
  docker::image { "${registry}/${user}/${mservice}":
    image_tag  => $image_tag,
    ensure     => latest,
  } ->

  ## runs image in host
  docker::run { "${mservice}:${image_tag}":
    image            => "${registry}/${user}/${mservice}:${image_tag}",
    pull_on_start    => true,
    memory_limit     => '128m',
    #ports            => ["${hosts::params::speiseplan_a::internal_ip}:${expose}:${port}"],
    ports            => ["${expose}:${port}"],
    restart_service  => true,
    force_restart    => true,
    command          => $command,
    ## mwolf runs with mservice user to gain read access to git repositories
    username         => $service_user,
    hostentries      => [
      "${hosts::params::speiseplan_a::hostname}:${hosts::params::speiseplan_a::internal_ip}",
      "${hosts::params::speiseplan_b::hostname}:${hosts::params::speiseplan_b::internal_ip}",
      # links
      # _self
      "${hosts::params::speiseplan_a::mwolf}:${hosts::params::speiseplan_a::internal_ip}",
      # config server
      "${hosts::params::speiseplan_a::mconfig}:${hosts::params::speiseplan_a::internal_ip}",
      # eureka server registry
      "${hosts::params::speiseplan_a::markimedes}:${hosts::params::speiseplan_a::internal_ip}",
      # services
      "${hosts::params::speiseplan_b::rabbitmq}:${hosts::params::speiseplan_b::internal_ip}",
    ],
  }
}