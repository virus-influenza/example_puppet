class profiles::docker::images::mwolf::package(
){

  require profiles::docker::images::mwolf::params

  $mservice   = $profiles::docker::images::mwolf::params::mservice
  $revision   = $profiles::docker::images::mwolf::params::image_tag

  $build      = $profiles::docker::images::mwolf::params::build
  $build_opts = $profiles::docker::images::mwolf::params::build_opts

  # create and distribute packaged microservice
  microservice::util::build { "${mservice}":
    mservice    => $mservice,
    revision    => $revision,
    build       => $build,
    build_opts  => $build_opts,
    deliver_to  => "/apps",
    # require     => Class['Profiles::Docker::Base::Image'] # seems to not work properly
  }

}