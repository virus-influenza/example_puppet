class profiles::docker::images::mwolf::params(
){
  # docker image
  $mservice     = 'mwolf'
  $image_tag    = 'unstable'
  $port         = '8080'
  $expose       = '10501'
  $service_type = 'java'

  # registry
  $registry     = 'speiseplan.menu:5000'
  $user         = 'virus'

  # service
  # build service
  $build        = 'gradle build'
  $build_opts   = ''

  # build box
  $puppet_code  = [
  # packages the mservice inside the docker image
    "include profiles::docker::images::mwolf::package",
  ]

  # running service
  $bin_opts     = '-Xmx128m'
  $service_opts = "--spring.profiles.active=prod \
  --spring.application.name=${mservice} \
  --spring.cloud.config.label=${image_tag} \
  --eureka.instance.nonSecurePort=${expose} \
  --eureka.instance.appname=${mservice}-${image_tag} \
  "
  $service_user = 'mservices'
}