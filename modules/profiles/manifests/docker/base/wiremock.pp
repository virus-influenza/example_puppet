class profiles::docker::base::wiremock(
){

  class{ 'users::fabric': } ->

  # wiremock
  class { 'paths::docker::wiremock': } ->

  # wiremock
  class { 'meinwiremock': }

  # mappings and __files folders from repository
  # mappings now in host and mounted volume in image
  # class {'units::wiremock_mappings': }
}