class profiles::docker::base::image(
){

  class{ 'users': } ->

  class{ 'meinlocales::base': }->

  # paths
  class { 'paths::docker::base': } ->
  class { 'paths::docker::images': } ->
  class { 'paths::docker::apps':} ->
  class { 'paths::binaries': } ->
  class { 'paths::repositories': } ->
  class { 'paths::packages': } ->

  # git
  class { 'meingit': }
}
