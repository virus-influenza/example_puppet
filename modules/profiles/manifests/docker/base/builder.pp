class profiles::docker::base::builder(
){

  # build tools for jenkins machine, for now it goes here
  # java
  class{ 'meinjava':
    package => 'openjdk-7-jdk'
  } ->
  class{ 'gradle': } ->

    # node
    # node and npm
  class{ "nodejs::install":
    version =>  "v5.2.0",
    source  => "https://nodejs.org/dist/v5.2.0/"
  } ->

    # global packages
    # gulp
  nodejs::package{ 'gulp':
    name         => 'gulp',
    options      => '--global',
    node_version => "v5.2.0"
  }->

    # babel
  nodejs::package{ 'babel':
    name         => 'babel',
    options      => '--global',
    node_version => "v5.2.0"
  }->

    # browser sync
  nodejs::package{ 'browser-sync':
    name         => 'browser-sync',
    options      => '--global',
    node_version => "v5.2.0"
  }->

    # required by npm
  package{ "ensure make present":
    ensure => present,
    name   => "make",
  }->

    # required by npm
  package{ "ensure build-essential present":
    ensure => present,
    name   => "build-essential",
  }->

    # required by npm
  package{ "ensure libkrb5-dev present":
    ensure => present,
    name   => "libkrb5-dev",
  }
  # eof build tools
}
