class profiles::docker::base::host(
){

  # users
  # root & mservices with ssh deployment keys
  class{ 'users': } ->

  # debian and locales fun
  class{ 'meinlocales::base': }->

  # paths
  # /home/mservices/repos/units & /home/mservices/repos/services [mservices repos, static repos]
  class { 'paths::repositories': } ->
  # /home/mservices/docker/images [Dockerfile and puppet modules to build docker images]
  class { 'paths::docker::images': } ->

  # /opt/docker
  class { 'paths::docker::base': } ->
  # /opt/binaries
  class { 'paths::binaries': } ->
  # /opt/packages
  class { 'paths::packages': } ->
  # /opt/wiremock
  class { 'paths::wiremock': } ->

    # THIS GOES NOT HERE
    # profile for builder machine, containes build tools for microservices ( java, gradle, node, npm, gulp, make )
  class { "profiles::docker::base::builder": } ->

  # units
  # /modules -> /$UNITS_HOME/speiseplan-puppet
  class { 'units::puppet_base': } ->

    # /wiremock/__files   -> /$UNITS_HOME/api-cook/$mappings_tag/__files
  # /wiremock/mappijngs -> /$UNITS_HOME/api-cook/$mappings_tag/mappings
  class {'units::wiremock_mappings': } ->

  # puppet firewall module
  # class{'meinfirewall':} ->

  # docker
  class { 'meindocker::install':}
}
