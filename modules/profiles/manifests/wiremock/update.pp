class profiles::wiremock::update(
){
  class {'users::mservices' : } ->
  class {'paths::repositories' : } ->
  # /opt/wiremock
  class { 'paths::wiremock': } ->

  class {'units::wiremock_mappings' : }
}