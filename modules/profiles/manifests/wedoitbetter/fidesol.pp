class profiles::wedoitbetter::fidesol(

  $bare_path,
  $identifier,

  $seicu,
  $seicu_web,
  $seicu_core

){

#class{"paths::wedoitbetter::bare_repositories":} ->

# fidesol client
  file { "$bare_path/$identifier":
    ensure  => directory,
    owner   => 'git',
    group   => 'git',
    mode    => 644,
  } ->

  vcsrepo { "assert $seicu bare repo created":
    ensure   => bare,
    provider => git,
    user     => git,
    path     => "$bare_path/$identifier/$seicu",
    require  => User["git"]
  } ->

  vcsrepo { "assert $seicu_web bare repo created":
    ensure   => bare,
    provider => git,
    user     => git,
    path     => "$bare_path/$identifier/$seicu_web",
    require  => User["git"]
  } ->

  vcsrepo { "assert $seicu_core bare repo created":
    ensure   => bare,
    provider => git,
    user     => git,
    path     => "$bare_path/$identifier/$seicu_core",
    require  => User["git"]
  }

}