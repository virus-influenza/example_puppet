class profiles::wedoitbetter::base(

){

  class {'users::mservices': } ->
  # root and access keys
  class {'users::wedoitbetter::root': } ->
  # git and access keys
  class {'users::wedoitbetter::git': } ->

  class{ 'paths::repositories': } ->
  class{ 'paths::wedoitbetter::bare_repositories': } ->

  # git
  class { 'meingit': } ->

  # java
  class{ 'meinjava':
    package => 'openjdk-7-jdk'
  }->

  # creates puppet repository and convenience /modules link for puppet apply
  class{ 'units::puppet_base': }
}