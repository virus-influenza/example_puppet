class profiles::wedoitbetter::repositories(

  $bare_path,
  $origin,

){

  class{ "profiles::wedoitbetter::fidesol": } ->

  class{"profiles::wedoitbetter::micsa": } ->

  class{"profiles::wedoitbetter::wedoitbetter": } ->

  dahood::linka{
    "repositories link for easy cloning $bare_path to $origin":
      link   => $origin,
      target => "$bare_path"
  }
}