class profiles::wedoitbetter::micsa(

  $bare_path,
  $identifier,

){

#class{"paths::wedoitbetter::bare_repositories":} ->

# micsa client
  file { "$bare_path/$identifier":
    ensure  => directory,
    owner   => 'git',
    group   => 'git',
    mode    => 644,
  }

  # for misa repositories
  # ->
  #vcsrepo { "assert $repo bare repo created":
  #  ensure   => bare,
  #  provider => git,
  #  user     => git,
  #  path     => "$bare_path/$seicu",
  #  require  => User["git"]
  #}

}