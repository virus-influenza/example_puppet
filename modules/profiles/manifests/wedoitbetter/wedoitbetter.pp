class profiles::wedoitbetter::wedoitbetter(

  $bare_path,
  $identifier,

  $subclients_cobuys,
  $subclients_cobuys_projects_puppet,

  $subclients_minions,
  $subclients_minions_projects_puppet,
){

  #class{"paths::wedoitbetter::bare_repositories":} ->

  # we are our own client
  file { "$bare_path/$identifier":
    ensure  => directory,
    owner   => 'git',
    group   => 'git',
    mode    => 644,
  } ->

  # suclient cobuys
  file { "$bare_path/$identifier/$subclients_cobuys":
    ensure  => directory,
    owner   => 'git',
    group   => 'git',
    mode    => 644,
  } ->

  vcsrepo { "assert $subclients_cobuys_projects_puppet bare repo created":
    ensure   => bare,
    provider => git,
    user     => git,
    path     => "$bare_path/$identifier/$subclients_cobuys/$subclients_cobuys_projects_puppet",
    require  => User["git"]
  }

  # suclient minions
  file { "$bare_path/$identifier/$subclients_minions":
    ensure  => directory,
    owner   => 'git',
    group   => 'git',
    mode    => 644,
  } ->

  vcsrepo { "assert $subclients_minions_projects_puppet bare repo created":
    ensure   => bare,
    provider => git,
    user     => git,
    path     => "$bare_path/$identifier/$subclients_minions/$subclients_minions_projects_puppet",
    require  => User["git"]
  }
}