class profiles::fabric::base(
){

  # python @see https://github.com/stankevich/puppet-python
  package{"python":
    ensure => latest
  }->

  # meinfabric new module with configuration files needed
  package{"fabric":
    ensure => latest
  }->

  # fabric enabled user
  class{"users::fabric": }->
  # repositories
  class { 'paths::repositories': } ->
  # fabric repository
  class{"units::fabric_base": }

}