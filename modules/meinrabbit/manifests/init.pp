class meinrabbit(

  $port              = 5672,
  $management_port   = 5673,
  $bind_ip           = '127.0.0.1',

  $delete_guest_user = true,
  $config_cluster    = false,
  $admin_enabled     = true,

){

  #https://forge.puppetlabs.com/puppetlabs/rabbitmq
  #include '::rabbitmq'
  class { '::rabbitmq':
    admin_enable            => $admin_enabled,
    config_cluster          => $config_cluster,
    delete_guest_user       => $delete_guest_user,
    management_port         => $management_port,
    port                    => $port,
    service_ensure          => running,
    service_manage          => true,
    environment_variables   => {
      'RABBITMQ_NODE_IP_ADDRESS' => $bind_ip,
    },
  }
}
